package com.mvp.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mvp.entity.DailyForm;
import com.mvp.entity.DailyFormItem;
import com.mvp.entity.Progress;
import com.mvp.mapper.DailyFormItemMapper;
import com.mvp.mapper.DailyFormMapper;
import com.mvp.mapper.ProgressMapper;
import com.mvp.mapper.UserMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static com.mvp.controller.ProgressController.fillAdvice;

@Component
public class DailyFormTask {

    @Resource
    private DailyFormMapper dailyFormMapper;

    @Resource
    private DailyFormItemMapper dailyFormItemMapper;

    @Resource
    private UserMapper userMapper;


    @PostConstruct
    public void init() throws InterruptedException {
        System.out.println("init");
        makeDailyFormTask();
    }

    @Scheduled(cron = "30 * * * * ?")
    public void makeDailyFormTask() throws InterruptedException {
        Random random = new Random();
        // 生成 0 - 10 之间的随机整数
        int randomNumber = random.nextInt(11);
        Thread.sleep(1000*randomNumber);
        System.out.println("---------makeDailyFormTask-----"+new Date());

        //先得到所有的用户
        List<String> usernames = userMapper.selectALlUserName();
        for (String username : usernames){
            QueryWrapper<DailyForm> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("username",username);
            queryWrapper.last("and (deleted = 0 or deleted is null) ");
            List<DailyForm> dailyFormList = dailyFormMapper.selectList(queryWrapper);
            //检查一下是否已经生成了表单
            //todo 如果当天创建了配置，生成当天的表单吗，我觉得最好是立刻生成一下
            //遍历
            Integer todayFormItemCount = dailyFormItemMapper.getTodayFormItemCount(username, new Date());
            System.out.println("打印一下查询的结果："+todayFormItemCount);

            if(todayFormItemCount == null || todayFormItemCount == 0){
                for(DailyForm dailyForm : dailyFormList){
                    int prototypeId = dailyForm.getId();
                    String formItemName = dailyForm.getName();
                    String formItemType = dailyForm.getType();
                    String formItemSelections = dailyForm.getSelections();
                    Integer formItemOrderNum = dailyForm.getOrderNum();
                    String formItemStat = dailyForm.getStat();//暂时不用

                    DailyFormItem dailyFormItem = new DailyFormItem();
                    dailyFormItem.setPrototypeId(prototypeId);
                    dailyFormItem.setUsername(username);
                    dailyFormItem.setName(formItemName);
                    dailyFormItem.setType(formItemType);
                    dailyFormItem.setOrderNum(formItemOrderNum);
                    dailyFormItem.setDateTime(new Date());

                    if(formItemType.equals("数值")){
                        dailyFormItem.setDataValue(null);

                    }else if(formItemType.equals("布尔")){
                        dailyFormItem.setBooleanValue(null);

                    }else if(formItemType.equals("选项")){
                        dailyFormItem.setSelections(formItemSelections);
                        dailyFormItem.setSelected(null);

                    }else if(formItemType.equals("时间戳")){
                        dailyFormItem.setDataTime(null);

                    }else if(formItemType.equals("字符串")){
                        dailyFormItem.setContent(null);

                    }
                    dailyFormItemMapper.insert(dailyFormItem);
                }
            }
        }
    }
}
