package com.mvp.task;
import com.mvp.entity.Progress;
import com.mvp.mapper.ProgressItemMapper;
import com.mvp.mapper.ProgressMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import static com.mvp.controller.ProgressController.fillAdvice;

@Component
public class MyTast {

    @Resource
    private ProgressMapper progressMapper;

    @PostConstruct
    public void init(){
        System.out.println("init");
        calculateAdvice();
    }

    //每1分钟打印一次hello world
//    @Scheduled(cron = "0 0 0 * * ?")
    @Scheduled(cron = "0 * * * * ?")
    public void calculateAdvice(){
        System.out.println("hello world"+new Date());
        //加载所有的progress
        List<Progress> progresses = progressMapper.selectList(null);
        for (Progress progress : progresses){
            //计算进度
            System.out.println(progress);
            fillAdvice(progress);
            System.out.println(progress);
            progressMapper.updateById(progress);

        }
    }
}
