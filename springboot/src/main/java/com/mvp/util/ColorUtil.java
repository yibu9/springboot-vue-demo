package com.mvp.util;

import org.springframework.stereotype.Component;

@Component
public class ColorUtil {

    public static String getColorByIndex(int index) {
        if (PRETTY_COLORS.length == 0) {
            throw new IllegalArgumentException("颜色数组为空");
        }
        return PRETTY_COLORS[Math.abs(index) % PRETTY_COLORS.length];
    }

    public static final String[] PRETTY_COLORS = {
            "#FF6B81", // Soft Raspberry
            "#F78888", // Light Coral
            "#FFA89E", // Peachy Pink
            "#FFD5C2", // Pale Salmon
            "#FFEBD6", // Warm Beige
            "#FFDEBA", // Light Gold
            "#FFEAB4", // Soft Yellow
            "#FFF3CC", // Pale Yellow
            "#FFFAD0", // Off White
//            "#FFFFFF", // Pure White

            "#3498db", // Bright Blue
            "#2ecc71", // Nephritis
            "#e74c3c", // Alizarin
            "#9b59b6", // Amethyst
            "#f1c40f", // Goldenrod
            "#1abc9c", // Turquoise
            "#34495e", // Midnight Blue
            "#95a5a6", // Asbestos
            "#e67e22", // Carrot
            "#2c3e50", // Dark Slate

            "#FF6384", // Raspberry
            "#36A2EB", // Sky Blue
            "#FFCE56", // Gold
            "#4BC0C0", // Turquoise
            "#9966FF", // Violet
            "#FF9F40", // Tangerine
            "#3ADF00", // Lime Green
            "#FF3737", // Crimson
            "#6610F2", // Indigo
            "#00BFA5"  // Cyan
    };
}
