package com.mvp.util;

import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

@Component
public class TimeUtil {

    public static LocalDate getStartOfWeek(LocalDate date) {
        return date.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    }

    public static LocalDate getEndOfWeek(LocalDate date) {
        return date.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
    }

    public static LocalDate getStartOfMonth(LocalDate date) {
        return date.with(TemporalAdjusters.firstDayOfMonth());
    }

    public static LocalDate getEndOfMonth(LocalDate date) {
        return date.with(TemporalAdjusters.lastDayOfMonth());
    }

    public static LocalDate getStartOfQuarter(LocalDate date) {
        int currentMonth = date.getMonthValue();
        int quarterStartMonth = ((currentMonth - 1) / 3) * 3 + 1;
        return date.withMonth(quarterStartMonth).with(TemporalAdjusters.firstDayOfMonth());
    }

    public static LocalDate getEndOfQuarter(LocalDate date) {
        int currentMonth = date.getMonthValue();
        int quarterEndMonth = ((currentMonth - 1) / 3) * 3 + 3;
        return date.withMonth(quarterEndMonth).with(TemporalAdjusters.lastDayOfMonth());
    }

    public static LocalDate getStartOfYear(LocalDate date) {
        return date.with(TemporalAdjusters.firstDayOfYear());
    }

    public static LocalDate getEndOfYear(LocalDate date) {
        return date.with(TemporalAdjusters.lastDayOfYear());
    }
    // 获取去年的时间范围
    public static LocalDate[] getLastYearRange() {
        LocalDate today = LocalDate.now();
        LocalDate startOfYear = getStartOfYear(today.minusYears(1));
        LocalDate endOfYear = getEndOfYear(today.minusYears(1));
        return new LocalDate[]{startOfYear, endOfYear};
    }
    // 获取上周的时间范围
    public static LocalDate[] getLastWeekRange() {
        LocalDate today = LocalDate.now();
        LocalDate startOfWeek = getStartOfWeek(today.minusWeeks(1));
        LocalDate endOfWeek = getEndOfWeek(today.minusWeeks(1));
        return new LocalDate[]{startOfWeek, endOfWeek};
    }

    // 获取上个月的时间范围
    public static LocalDate[] getLastMonthRange() {
        LocalDate today = LocalDate.now();
        LocalDate startOfMonth = getStartOfMonth(today.minusMonths(1));
        LocalDate endOfMonth = getEndOfMonth(today.minusMonths(1));
        return new LocalDate[]{startOfMonth, endOfMonth};
    }

    // 获取上个季度的时间范围
    public static LocalDate[] getLastQuarterRange() {
        LocalDate today = LocalDate.now();
        LocalDate startOfLastQuarter = getStartOfQuarter(today.minusMonths(3));
        LocalDate endOfLastQuarter = getEndOfQuarter(today.minusMonths(3));
        return new LocalDate[]{startOfLastQuarter, endOfLastQuarter};
    }

    //传入分钟，给你返回xx小时yy分钟的字符串
    public  static String formatMinutesToHourMin(int minutes) {
        if (minutes < 0) {
            throw new IllegalArgumentException("分钟数不能为负");
        }
        int hours = minutes / 60; // 计算完整小时数
        int remainingMinutes = minutes % 60; // 计算剩余分钟数
        StringBuilder timeBuilder = new StringBuilder();
        // 添加小时部分（如果有的话）
        if (hours > 0) {
            timeBuilder.append(hours).append("小时");
        }
        // 添加分钟部分（如果有的话）
        if (remainingMinutes > 0 || hours == 0) { // 如果有分钟或者没有小时（即小于一小时）
            if (hours > 0) {
                //timeBuilder.append(""); // 在小时和分钟之间添加空格
            }
            timeBuilder.append(remainingMinutes).append("分钟");
        } else if (hours > 0 && remainingMinutes == 0) {
            timeBuilder.append("整"); // 当正好是整小时时可以加上“整”
        }
        return timeBuilder.toString();
    }
}
