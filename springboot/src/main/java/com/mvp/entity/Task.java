package com.mvp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;

@TableName("task")
@Data
public class Task {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    private String name;
    private String user;
    private String detail;
    private String taskType;
    private String father;
    private String importance;
    private String urgency;
    private Integer orderNum;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date ddl;
    private String associatedPerson;
    private String comment;
    private Integer estimatedTime;
    private String progress;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date finishedTime;
    private Integer level;
}