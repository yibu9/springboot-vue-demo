package com.mvp.entity.dto;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Data
@Accessors(chain = true)
@TableName("t_progress")
public class ProgressDTO {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private String user;
    private Double targetValue;
    private Double currentValue;
    private LocalDateTime createTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    private OffsetDateTime expectedCompletionTime;
    private String status;
    private String content;
    private String evaluation;
    private String unit;
    private Integer orderNum;
    private String type;
}