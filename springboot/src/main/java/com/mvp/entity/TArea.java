package com.mvp.entity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_area")
public class TArea {

    private Integer id;

    private Long parentId;

    private String ancestors;

    private String areaName;

    private Long lineId;

    private String alias;

    private String description;

    private Integer orderNum;

    private Integer nodeType;

    private Integer isDelete;

    private Integer isDisplay;

    private String creator;

    private java.util.Date createTime;

    private String updater;

    private java.util.Date updateTime;

    private Long orgId;

    private String orgAncestors;
}