package com.mvp.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Timestamp;
import com.baomidou.mybatisplus.annotation.IdType;


@Data
@TableName("subscription_item")
public class SubscriptionItem {
    private String subscriptionType;
    private String name;
    private String username;
    private Integer sortWeight;
    private String remarks;
    private Timestamp creationTime;
    private String subscriptionTime;
    private Integer targetTime;
}