package com.mvp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;

@TableName("habit")
@Data
public class Habit {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    private String name;
    private String user;
    private String type;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    private String stat;
    private String orderNum;
    private String comment;
    private String isDeleted;
}