package com.mvp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;

@TableName("habit_log")
@Data
public class HabitLog {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date dateTime;
    private Integer habitId;
    private String habitName;
    private String stat;
    private String user;
    private String type;
    private String content;
    private String comment;

}