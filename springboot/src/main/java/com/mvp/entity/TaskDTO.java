package com.mvp.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;


@Data
public class TaskDTO {
    private Integer id;
    private String name;
    private String user;
    private String detail;
    private String taskType;
    private String father;
    private String importance;
    private String urgency;
    private Integer orderNum;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date ddl;
    private String associatedPerson;
    private String comment;
    private Integer estimatedTime;
    private String progress;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date finishedTime;
    private Integer level;
    private ArrayList<TaskDTO> children;


    public TaskDTO(Task task){
        this.id = task.getId();
        this.name = task.getName();
        this.user = task.getUser();
        this.detail = task.getDetail();
        this.taskType = task.getTaskType();
        this.father = task.getFather();
        this.importance = task.getImportance();
        this.urgency = task.getUrgency();
        this.orderNum = task.getOrderNum();
        this.ddl = task.getDdl();
        this.associatedPerson = task.getAssociatedPerson();
        this.comment = task.getComment();
        this.estimatedTime = task.getEstimatedTime();
        this.progress = task.getProgress();
        this.finishedTime = task.getFinishedTime();
        this.level = task.getLevel();
    }
}