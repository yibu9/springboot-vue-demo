package com.mvp.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@TableName("t_progress_item")
public class ProgressItem {
    @TableId(type = IdType.AUTO)
    private Integer id;
    @TableField("progress_id")
    private Integer progressId;
    private LocalDateTime dateTime;
    private String content;
    private Double value;
}