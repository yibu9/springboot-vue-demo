package com.mvp.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@TableName("t_progress")
public class Progress {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private String user;
    private Double targetValue;
    private Double currentValue;
    private Double percentage;
    private LocalDateTime createTime;
    private LocalDateTime expectedCompletionTime;
    private String status;
    private String content;
    private String evaluation;
    private String unit;
    private Integer orderNum;
    private String type;
    private Integer deleted;
    private Double expectedValue;
    private String statusMsg;
    private String advice;
    private Double diff;
    private Double adviceValue;
}