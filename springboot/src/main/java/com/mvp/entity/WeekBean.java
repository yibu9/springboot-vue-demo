package com.mvp.entity;

import lombok.Data;

@Data
public class WeekBean {

    private String largeCategory;
    private String year;
    private String weekNumber;
    private Double duration;
}
