package com.mvp.entity;

import lombok.Data;

import java.util.List;

@Data
public class PageResult<T> {

    private List<T> items;
    private long total;
    private int code;

    public PageResult(List<T> items, long total,int code) {
        this.items = items;
        this.total = total;
        this.code=code;
    }

    // Getters and Setters
    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
    public int getCode(){
        return code;
    }
    public void setCode(int code){
        this.code=code;
    }

}