package com.mvp.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.sql.Timestamp;

@Data
@TableName("daily_form")
public class DailyForm {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String username;
    private String name;
    private Timestamp createTime;
    private String type; // 对应于enum('数值','布尔','选项','时间戳','字符串')
    private String selections;
    private Double dataValue;
    private String content;
    private Timestamp dataTime;
    private Integer orderNum;
    private String stat; // 对应于enum('暂停','启用','删除')
    private Timestamp deletedAt;
    private Integer deleted;
}