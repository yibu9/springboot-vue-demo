package com.mvp.entity;

import lombok.Data;

@Data
public class ValueBean {
    private String user;
    private String date;
    private Double value;
}
