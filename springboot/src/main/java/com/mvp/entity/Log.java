package com.mvp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@TableName("t_log")
@Data
public class Log {

    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    private String user;
    private String largeCategory;
    private String type;
    private String project;
    private String detail;
    private String reportContent;
    private String dayOfWeek;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date DateTime;
    private String comment;
    private Integer duration;
    private Integer value;
    private Integer urgency;
    private Integer difficulty;
    private Integer focus;
    private Integer timing;
    private Double score;
}
