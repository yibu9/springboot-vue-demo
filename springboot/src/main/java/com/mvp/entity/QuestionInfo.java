package com.mvp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("question_info")
public class QuestionInfo {
    @TableId(type = IdType.AUTO)
    private Integer id;//题目的主键id
    private String userName;//用户名称
    private String questionTitle;//题目名称
    private String subject;//科目
    private String firstLevelCategory;//一级分类
    private String secondLevelCategory;//二级分类
    private String thirdLevelCategory;//三级分类
    private String questionType;//题目类型
    private String tags;//标签
    private String relatedKnowledgePoints;//相关知识点
    private String questionImage;//题目图片
    private String questionText;//题目文本
    private String hintText;//提示文本
    private String answerImage;//答案图片
    private String answerText;//答案文本
    private String difficultyLevel;//题目难度
    private String understandingDegree;//理解程度
    private BigDecimal questionValue;//题目价值
    private String questionStatus;//题目状态
    private Integer consecutiveSuccessCount;//连续正确次数
    private Integer consecutiveErrorCount;//连续错误次数
    private Integer cumulativeErrorCount; //累计错误次数
    private Integer cumulativeSuccessCount;//累计正确次数
    private Integer totalAttemptCount;//总尝试次数
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastAttemptTime;//最后尝试时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date queryAvailableTime;//查询可用时间
    private String insights;//反思
}