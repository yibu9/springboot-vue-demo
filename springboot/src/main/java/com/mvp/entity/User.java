package com.mvp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("user")
@Data
public class User {

    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    private String username;
    private String password;
    private String nickName;
    private String age;
    private String sex;
    private String address;
    private Integer role;

}
