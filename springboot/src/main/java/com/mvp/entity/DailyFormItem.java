package com.mvp.entity;
import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("daily_form_item")
public class DailyFormItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer prototypeId;
    private String username;
    private String name;
    private String type;
    private String selections;
    private String selected;
    private Double dataValue;
    private String content;
    private LocalDateTime dataTime;
    private LocalDateTime updateTime;
    private Boolean used;
    private Date dateTime;
    private String booleanValue;
    private String value;
    private Integer orderNum;
    private Integer deleted;
}