package com.mvp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@TableName("journal")
@Data
public class Journal {

    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    private String user;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date DateTime;
    private String content;
    private String comment;
    private String privilege;
}
