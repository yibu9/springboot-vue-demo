package com.mvp.controller;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.mvp.common.Result;
import com.mvp.entity.Log;
import com.mvp.mapper.LogMapper;
import com.mvp.util.ColorUtil;
import com.mvp.util.TimeUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/statQuality")
public class StatQualityController {
    @Resource
    private LogMapper logMapper;

    private Map<String,String> colorMap = new HashMap<>();

    @GetMapping("/getData")
    public Result<?> getData(
            @RequestParam(required = true) String username
    ) {
        //校验用户名
        if(StringUtils.isBlank(username)){
            return Result.error("-1","用户名不能为空");
        }
        JSONObject res = new JSONObject();
        LocalDate now = LocalDate.now();

        //本周
        LocalDate[] thisWeekRange = {TimeUtil.getStartOfWeek(now), TimeUtil.getEndOfWeek(now)};

        List<Log> thisWeekDifficultyData = getDataInRange("difficulty",username,thisWeekRange[0], thisWeekRange[1]);
        List<Log> thisWeekUrgencyData = getDataInRange("urgency",username,thisWeekRange[0], thisWeekRange[1]);
        List<Log> thisWeekFocusData = getDataInRange("focus",username,thisWeekRange[0], thisWeekRange[1]);
        List<Log> thisWeekValueData = getDataInRange("value",username,thisWeekRange[0], thisWeekRange[1]);
        //得到本周已经进行的天数
        long betweenThisWeek = ChronoUnit.DAYS.between(thisWeekRange[0], now);
        fillData(res,thisWeekValueData,
                thisWeekDifficultyData,
                thisWeekUrgencyData,
                thisWeekFocusData,
                "thisWeek",betweenThisWeek);

        // 获取本月的数据
        LocalDate[] thisMonthRange = {TimeUtil.getStartOfMonth(now), TimeUtil.getEndOfMonth(now)};
        List<Log> thisMonthValueData = getDataInRange("value",username,thisMonthRange[0], thisMonthRange[1]);
        List<Log> thisMonthDifficultyData = getDataInRange("difficulty",username,thisMonthRange[0], thisMonthRange[1]);
        List<Log> thisMonthUrgencyData = getDataInRange("urgency",username,thisMonthRange[0], thisMonthRange[1]);
        List<Log> thisMonthFocusData = getDataInRange("focus",username,thisMonthRange[0], thisMonthRange[1]);
        //得到本月已经进行的天数
        long betweenThisMonth = ChronoUnit.DAYS.between(thisMonthRange[0], now);
        fillData(res,thisMonthValueData,
                thisMonthDifficultyData,
                thisMonthUrgencyData,
                thisMonthFocusData,
                "thisMonth",betweenThisMonth);

        System.out.println("------------categoryData.res-------");
        System.out.println(res);
        return Result.success(res);
    }



    public List<Log> getDataInRange(String field,String username , LocalDate startDate, LocalDate endDate)   {
        List<Log> Logs = logMapper.selectQualityByRange(field,username, startDate, endDate);
        return Logs;
    }

    public void fillData(JSONObject result,
                         List<Log> thisValueData,
                         List<Log> thisDifficultyData,
                         List<Log> thisUrgencyData,
                         List<Log> thisFocusData,
                         String dataName,long betweenDays){
        System.out.println("---------------thisValueData----------------");
        System.out.println(thisValueData);


        HashMap<String,Integer> valueMap = new HashMap<>();
        for (Log log : thisValueData){
            Integer value = log.getValue();
            if(value == 1){
                valueMap.put("低价值",log.getDuration());
            }else if(value == 2){
                valueMap.put("中价值",log.getDuration());
            }else if(value == 3){
                valueMap.put("高价值",log.getDuration());
            }else if(value == 4){
                valueMap.put("极高价值",log.getDuration());
            }
        }
        if(!valueMap.containsKey("低价值")){
            valueMap.put("低价值",0);
        }
        if(!valueMap.containsKey("中价值")){
            valueMap.put("中价值",0);
        }
        if(!valueMap.containsKey("高价值")){
            valueMap.put("高价值",0);
        }
        if(!valueMap.containsKey("极高价值")){
            valueMap.put("极高价值",0);
        }
        System.out.println("valueMap");
        System.out.println(valueMap);

        HashMap<String,Integer> difficultyMap = new HashMap<>();
        for (Log log : thisDifficultyData){
            Integer difficulty = log.getDifficulty();
            if(difficulty == 1){
                difficultyMap.put("简单",log.getDuration());
            }else if(difficulty == 2){
                difficultyMap.put("一般",log.getDuration());
            }else if(difficulty == 3){
                difficultyMap.put("困难",log.getDuration());
            }else if(difficulty == 4){
                difficultyMap.put("极难",log.getDuration());
            }
            }
        if(!difficultyMap.containsKey("简单")){
            difficultyMap.put("简单",0);
        }
        if(!difficultyMap.containsKey("一般")){
            difficultyMap.put("一般",0);
        }
        if(!difficultyMap.containsKey("困难")){
            difficultyMap.put("困难",0);
        }
        if(!difficultyMap.containsKey("极难")){
            difficultyMap.put("极难",0);
        }

        HashMap<String,Integer> urgencyMap = new HashMap<>();
        for (Log log : thisUrgencyData){
            Integer urgency = log.getUrgency();
            if(urgency == 1){
                urgencyMap.put("不紧急",log.getDuration());
            }else if(urgency == 2){
                urgencyMap.put("一般",log.getDuration());
            }else if(urgency == 3){
                urgencyMap.put("紧急",log.getDuration());
            }else if(urgency == 4){
                urgencyMap.put("非常紧急",log.getDuration());
           }
        }
        if(!urgencyMap.containsKey("不紧急")){
            urgencyMap.put("不紧急",0);
        }
        if(!urgencyMap.containsKey("一般")){
            urgencyMap.put("一般",0);
        }
        if(!urgencyMap.containsKey("紧急")){
            urgencyMap.put("紧急",0);
        }
        if(!urgencyMap.containsKey("非常紧急")){
            urgencyMap.put("非常紧急",0);
        }
        HashMap<String,Integer> focusMap = new HashMap<>();
        for (Log log : thisFocusData){

            Integer focus = log.getFocus();
            if(focus == 1){
                focusMap.put("不专注",log.getDuration());
            }else if(focus == 2){
                focusMap.put("一般",log.getDuration());
            }else if(focus == 3){
                focusMap.put("专注",log.getDuration());
            }else if(focus == 4){
                focusMap.put("极度专注",log.getDuration());
            }
        }
        if(!focusMap.containsKey("不专注")){
            focusMap.put("不专注",0);
        }
        if(!focusMap.containsKey("一般")){
            focusMap.put("一般",0);
        }
        if(!focusMap.containsKey("专注")){
            focusMap.put("专注",0);
        }
        if(!focusMap.containsKey("极度专注")){
            focusMap.put("极度专注",0);
        }
        int rate ;
        int index;
        int sumDuration;
        JSONArray ValueArr = new JSONArray();
        Set<String> valueStrings = valueMap.keySet();
        rate = 0;
        index = 1;
        sumDuration = 0;
        for (String key : valueStrings) {
            rate += index*valueMap.get(key);
            index ++;
            sumDuration += valueMap.get(key);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",key);
            jsonObject.put("value",valueMap.get(key));
            jsonObject.put("itemStyle",colorObj(key));
            ValueArr.add(jsonObject);
        }
        result.set(dataName+"ValueRate",(double)rate/sumDuration);
        result.set(dataName+"Value",ValueArr);

        JSONArray DifficultyArr = new JSONArray();
        Set<String> difficultyStrings = difficultyMap.keySet();
        rate = 0;
        index = 1;
        sumDuration = 0;
        for (String key : difficultyStrings) {
            rate += index*difficultyMap.get(key);
            index ++;
            sumDuration += difficultyMap.get(key);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",key);
            jsonObject.put("value",difficultyMap.get(key));
            jsonObject.put("itemStyle",colorObj(key));
            DifficultyArr.add(jsonObject);
        }
        System.out.println("rate:"+rate);
        System.out.println("sumDuration"+sumDuration);
        result.set(dataName+"DifficultyRate",(double)rate/sumDuration);
        result.set(dataName+"Difficulty",DifficultyArr);

        JSONArray urgencyArr = new JSONArray();
        Set<String> urgencyStrings = urgencyMap.keySet();
        rate = 0;
        index = 1;
        sumDuration = 0;
        for (String key : urgencyStrings) {
            rate += index*urgencyMap.get(key);
            index ++;
            sumDuration += urgencyMap.get(key);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",key);
            jsonObject.put("value",urgencyMap.get(key));
            jsonObject.put("itemStyle",colorObj(key));
            urgencyArr.add(jsonObject);
        }
        result.set(dataName+"UrgencyRate",(double)rate/sumDuration);
        result.set(dataName+"Urgency",urgencyArr);

        JSONArray focusArr = new JSONArray();
        Set<String> focusStrings = focusMap.keySet();
        rate = 0;
        index = 1;
        sumDuration = 0;
        for (String key : focusStrings) {
            rate += index*focusMap.get(key);
            index ++;
            sumDuration += focusMap.get(key);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",key);
            jsonObject.put("value",focusMap.get(key));
            jsonObject.put("itemStyle",colorObj(key));
            focusArr.add(jsonObject);
        }
        result.set(dataName+"FocusRate",(double)rate/sumDuration);
        result.set(dataName+"Focus",focusArr);
    }

    public JSONObject colorObj(String name){
        String color =  ColorUtil.getColorByIndex(6);
        if("极高价值".equals(name) || "极难".equals(name)  || "非常紧急".equals(name)  || "极度专注".equals(name) ){
             color =  ColorUtil.getColorByIndex(0);
        }else if("高价值".equals(name) || "困难".equals(name)  || "专注".equals(name)|| "紧急".equals(name) ){
             color =  ColorUtil.getColorByIndex(1);
        }else if("中价值".equals(name) || "一般".equals(name)){
             color =  ColorUtil.getColorByIndex(2);
        }else if("低价值".equals(name) || "简单".equals(name) || "不紧急".equals(name)){
            color =  ColorUtil.getColorByIndex(4);
        }else {
             color =  ColorUtil.getColorByIndex(5);
        }
        JSONObject obj = new JSONObject();
        obj.put("color",color);
        return obj;
    }


}
