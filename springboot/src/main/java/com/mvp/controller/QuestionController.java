package com.mvp.controller;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mvp.common.Result;
import com.mvp.entity.QuestionInfo;
import com.mvp.mapper.QuestionMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/question")
public class QuestionController {

    @Resource
    private QuestionMapper questionMapper;

    @PostMapping
    public Result<?> save(@RequestBody QuestionInfo question) {
        questionMapper.insert(question);
        return Result.success();
    }

    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Integer id) {
        questionMapper.deleteById(id);
        return Result.success();
    }

    @PutMapping
    public Result<?> update(@RequestBody QuestionInfo question) {
        questionMapper.updateById(question);
        return Result.success();
    }

    @GetMapping
    public Result<?> findPage(
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(required = false) String userName,
            @RequestParam(required = false) String questionTitle,
            @RequestParam(required = false) String subject,
            @RequestParam(required = false) String firstLevelCategory,
            @RequestParam(required = false) String questionType,
            @RequestParam(required = false) String difficultyLevel,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {

        LambdaQueryWrapper<QuestionInfo> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StringUtils.isNotBlank(userName), QuestionInfo::getUserName, userName)
                .like(StringUtils.isNotBlank(questionTitle), QuestionInfo::getQuestionTitle, questionTitle)
                .eq(StringUtils.isNotBlank(subject), QuestionInfo::getSubject, subject)
                .eq(StringUtils.isNotBlank(firstLevelCategory), QuestionInfo::getFirstLevelCategory, firstLevelCategory)
                .eq(StringUtils.isNotBlank(questionType), QuestionInfo::getQuestionType, questionType)
                .eq(StringUtils.isNotBlank(difficultyLevel), QuestionInfo::getDifficultyLevel, difficultyLevel)
                .ge(startDate != null, QuestionInfo::getLastAttemptTime, startDate)
                .le(endDate != null, QuestionInfo::getLastAttemptTime, endDate)
                .orderByDesc(QuestionInfo::getLastAttemptTime);

        return Result.success(questionMapper.selectPage(new Page<>(pageNum, pageSize), wrapper));
    }

    @GetMapping("/filters")
    public Result<?> getFilters(@RequestParam String userName) {
        JSONObject filters = new JSONObject();
        filters.set("subjects", questionMapper.getSubjectsByUser(userName));
        filters.set("categories", questionMapper.getFirstLevelCategories(userName));
        return Result.success(filters);
    }

    @PostMapping("/upload-image")
    public Result uploadImage(@RequestParam("image") MultipartFile file) {
        try {
            // 使用项目根目录下的 uploads/images 目录
            String projectRoot = System.getProperty("user.dir");
            String uploadDir = projectRoot + File.separator + "uploads" + File.separator + "images" + File.separator;

            File dir = new File(uploadDir);
            if (!dir.exists()) {
                dir.mkdirs();  // 递归创建目录
            }

            String fileName = UUID.randomUUID() + "_" + file.getOriginalFilename();
            File dest = new File(uploadDir + fileName);
            file.transferTo(dest);  // 保存文件

            return Result.success(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("-1", "图片上传失败");
        }
    }
}