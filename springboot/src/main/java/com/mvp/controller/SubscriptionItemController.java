package com.mvp.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mvp.common.Result;
import com.mvp.entity.SubscriptionItem;
import com.mvp.mapper.SubscriptionItemMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/subscriptionItem")
public class SubscriptionItemController {

    @Resource
    private SubscriptionItemMapper subscriptionItemMapper;

    @PostMapping
    public Result<?> save(@RequestBody SubscriptionItem item) {
        System.out.println("----------------save item:"+item);
        //对每一个属性进行校验
        if(StringUtils.isBlank(item.getSubscriptionType())){
            return Result.error("400","订阅类型不能为空");
        }

        if(StringUtils.isBlank(item.getName())){
            return Result.error("400","名称不能为空");
        }

        if(StringUtils.isBlank(item.getSubscriptionTime())){
            return Result.error("400","订阅周期不能为空");
        }

        if(item.getTargetTime() == null || item.getTargetTime()<=0){
            return Result.error("400","请填写正确的目标时间！");
        }

        if(item.getSortWeight() == null){
            item.setSortWeight(1000);
        }

        int insert = 0;
        try{
            insert = subscriptionItemMapper.insert(item);
        }catch (Exception e){
            return Result.error("-1","新增失败,已经存在该记录");
        }
        if(insert>0){
            return Result.success();
        }else{
            return Result.error("-1","新增失败");
        }
    }

    @DeleteMapping
    public Result<?> delete(
            @RequestParam String subscriptionType,
            @RequestParam String name,
            @RequestParam String username) {
        LambdaQueryWrapper<SubscriptionItem> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(SubscriptionItem::getSubscriptionType, subscriptionType)
                .eq(SubscriptionItem::getName, name)
                .eq(SubscriptionItem::getUsername, username);
        subscriptionItemMapper.delete(wrapper);
        return Result.success();
    }

    @PutMapping
    public Result<?> update(@RequestBody SubscriptionItem item) {

        System.out.println("----------------update item:"+item);
        //对每一个属性进行校验
        if(StringUtils.isBlank(item.getSubscriptionType())){
            return Result.error("400","订阅类型不能为空");
        }

        if(StringUtils.isBlank(item.getName())){
            return Result.error("400","名称不能为空");
        }

        if(StringUtils.isBlank(item.getSubscriptionTime())){
            return Result.error("400","订阅周期不能为空");
        }

        if(item.getTargetTime() == null || item.getTargetTime()<=0){
            return Result.error("400","请填写正确的目标时间！");
        }

        if(item.getSortWeight() == null){
            item.setSortWeight(1000);
        }
        int update = 0;

        try{
            update = subscriptionItemMapper.updateByKeys(item);
        }catch (Exception e){
            return Result.error("-1","更新失败");
        }

        if(update>0){
            return Result.success();
        }else{
            return Result.error("-1","新增失败");
        }
    }
//需要解决两个问题，一个是更新的时候无名称，一个是更新的逻辑，需要重写sql
    @GetMapping
    public Result<?> findPage(
            @RequestParam(defaultValue = "") String username,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(required = false) String subscriptionType,
            @RequestParam(required = false) String subscriptionTime,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String remarks,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startCreateTime,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endCreateTime) {

        LambdaQueryWrapper<SubscriptionItem> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StringUtils.isNotBlank(subscriptionType), SubscriptionItem::getSubscriptionType, subscriptionType)
                .like(StringUtils.isNotBlank(name), SubscriptionItem::getName, name)
                .eq(StringUtils.isNotBlank(username), SubscriptionItem::getUsername, username)
                .eq(StringUtils.isNotBlank(subscriptionTime), SubscriptionItem::getSubscriptionTime, subscriptionTime)
                .like(StringUtils.isNotBlank(remarks), SubscriptionItem::getRemarks, remarks)
                .ge(startCreateTime != null, SubscriptionItem::getCreationTime, startCreateTime)
                .le(endCreateTime != null, SubscriptionItem::getCreationTime, endCreateTime)
                .orderByDesc(SubscriptionItem::getSortWeight);

        return Result.success(subscriptionItemMapper.selectPage(new Page<>(pageNum, pageSize), wrapper));
    }

    @GetMapping("/names")
    public Result<?> getNames(
            @RequestParam String subscriptionType,
            @RequestParam String username) {
        //可能需要三个查询语句了。分开查询，不然字段不能作为参数传进去。
        List<String> names;
        if("largeCategory".equals(subscriptionType)){
            names = subscriptionItemMapper.getNamesByLargeCategoryAndUser(username);
        }else if("type".equals(subscriptionType)){
            names = subscriptionItemMapper.getNamesByTypeAndUser(username);
        }else if ("project".equals(subscriptionType)){
            names = subscriptionItemMapper.getNamesByProjectAndUser(username);
        }else{
            return Result.error("400","subscriptionType参数错误");
        }
        return Result.success(names);
    }
}