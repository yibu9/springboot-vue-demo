package com.mvp.controller;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mvp.common.Result;
import com.mvp.entity.DailyForm;
import com.mvp.entity.DailyFormItem;
import com.mvp.entity.Log;
import com.mvp.mapper.DailyFormItemMapper;
import com.mvp.mapper.DailyFormMapper;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
@RestController
@RequestMapping("/dailyForms")
public class DailyFormController {

    @Resource
    private DailyFormMapper dailyFormMapper;
    @Resource
    private DailyFormItemMapper dailyFormItemMapper;
    @GetMapping
    public Result<?> getAll(@RequestParam(defaultValue = "1") Integer pageNum,
                            @RequestParam(defaultValue = "10") Integer pageSize,
                            @RequestParam(defaultValue = "") String username) {
        System.out.println("----------getAll-------------");
        System.out.println("pageNum:"+pageNum);
        System.out.println("pageSize:"+pageSize);
        System.out.println("username:"+username);
        LambdaQueryWrapper<DailyForm> wrapper = Wrappers.<DailyForm>lambdaQuery();

        if(StringUtils.isNotBlank(username)){
            wrapper.eq(DailyForm::getUsername, username);
        }else{
            wrapper.eq(DailyForm::getUsername, "unknown");
            System.out.println("用户名未知");
        }
        wrapper.last("and (deleted = 0 or deleted is null) ");
//        wrapper.last("ORDER BY order_num DESC");
        Page<DailyForm> dailyForms = dailyFormMapper.selectPage(new Page<>(pageNum, pageSize),wrapper );
        return Result.success(dailyForms);
    }

    @PostMapping
    public Result<?> add(@RequestBody DailyForm dailyForm) {
        System.out.println("----------add-------------");
        System.out.println(dailyForm);
        String stat = dailyForm.getStat();
        if(StringUtils.isBlank(stat) || stat.equals("0")){
            stat = "启用";
            dailyForm.setStat(stat);
        }
        String selections = dailyForm.getSelections();
        if(StringUtils.isNotBlank(selections)){
            selections = selections.replace("，",",");
            dailyForm.setSelections(selections);
        }
        int insert = dailyFormMapper.insert(dailyForm);
        if(insert>0){
            int prototypeId = dailyForm.getId();
            String name = dailyForm.getName();
            String username = dailyForm.getUsername();
            String type = dailyForm.getType();
            DailyForm curAddedDailyForm = dailyFormMapper.selectDailyFormByFourField(prototypeId,name, username, type);
            createItem(curAddedDailyForm,username);
            return Result.success();
        }else{
            return Result.error("-1","添加失败");
        }
    }

    public void createItem(DailyForm dailyForm,String username){
        String formItemName = dailyForm.getName();
        String formItemType = dailyForm.getType();
        String formItemSelections = dailyForm.getSelections();
        Integer formItemOrderNum = dailyForm.getOrderNum();
        String formItemStat = dailyForm.getStat();//暂时不用

        DailyFormItem dailyFormItem = new DailyFormItem();
        dailyFormItem.setUsername(username);
        dailyFormItem.setName(formItemName);
        dailyFormItem.setType(formItemType);
        dailyFormItem.setOrderNum(formItemOrderNum);
        dailyFormItem.setDateTime(new Date());
        dailyFormItem.setPrototypeId(dailyForm.getId());

        if(formItemType.equals("数值")){
            dailyFormItem.setDataValue(null);

        }else if(formItemType.equals("布尔")){
            dailyFormItem.setBooleanValue(null);

        }else if(formItemType.equals("选项")){
            dailyFormItem.setSelections(formItemSelections);
            dailyFormItem.setSelected(null);

        }else if(formItemType.equals("时间戳")){
            dailyFormItem.setDataTime(null);

        }else if(formItemType.equals("字符串")){
            dailyFormItem.setContent(null);

        }
        System.out.println("即将插入对象："+dailyFormItem);
        dailyFormItemMapper.insert(dailyFormItem);
    }

    @PutMapping("/{id}")
    public Result<?> update(@PathVariable Integer id, @RequestBody DailyForm dailyForm) {
        System.out.println("----------update-------------");
        dailyForm.setId(id);
        int i = dailyFormMapper.updateById(dailyForm);
        if(i>0){
            return Result.success();
        }else{
            return Result.error("-1","添加失败");
        }
    }

    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Integer id) {
        System.out.println("----------delete-------------");
        int i = dailyFormMapper.deleteByPrototypeId(id);
        System.out.println("删除的结果："+i);
        if(i>0){
            dailyFormItemMapper.deleteByPrototypeId(id);
            return Result.success();
        }else{
            return Result.error("-1","添加失败");
        }
    }

//    public static void main(String[] args) {
//        String code = "你好，这是一个，东西,看看行不行,ok吗，hhaode,";
//        System.out.println(code);
//        code = code.replace("，",",");
//        System.out.println(code);
//    }
}