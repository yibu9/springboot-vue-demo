package com.mvp.controller;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.mvp.common.PubFun;
import com.mvp.common.Result;
import com.mvp.entity.Log;
import com.mvp.entity.WeekBean;
import com.mvp.mapper.LogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/weekstat")
public class WeekStatController {
    @Resource
    private LogMapper logMapper;


    @GetMapping("/list")
    public Result<?> weekstatlist(@RequestParam(required = true) String username) {
        List<WeekBean> weekList = logMapper.getWeekList(username);

        Set<String> categorySet = new HashSet<>();
        TreeSet<String> weekSet = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.compare(Integer.parseInt(o1), Integer.parseInt(o2));
            }
        });
        HashMap<String, List<WeekBean> > dataMap = new HashMap<>();

        for(WeekBean weekBean:weekList){
            String year = weekBean.getYear();
            String weekNumber = weekBean.getWeekNumber();
            String key = weekNumber;
            categorySet.add(weekBean.getLargeCategory());
            weekSet.add(key);
            if(dataMap.containsKey(key)){
                List<WeekBean> list = dataMap.get(key);
                list.add(weekBean);
                dataMap.put(key,list);
            }else{
                List<WeekBean> list = new ArrayList<>();
                list.add(weekBean);
                dataMap.put(key,list);
            }
        }
        categorySet.add("总时间");


        List <String> weekListForRes = new ArrayList<>(weekSet);
        List <String> categoryListForRes = new ArrayList<>(categorySet);

        JSONObject res = new JSONObject();
        JSONObject data = new JSONObject();
        res.set("categories", categoryListForRes);
        res.set("weeks", weekListForRes);
        res.set("data", data);

        System.out.println("dataMap:"+dataMap);
        JSONArray sumDoubleList = new JSONArray();
        data.put("总时间",sumDoubleList);

        //遍历每个week
        for(String week:weekSet){
            System.out.println("week:"+week);
            List<WeekBean> list = dataMap.get(week);
            System.out.println("list:"+list);
            //得到了这个week的数据,然后遍历每个category，如果能够匹配上，就把数据加进去，否则就加0进去
            double sum = 0;
            for(String category:categoryListForRes){
                System.out.println("category:"+category);
                JSONArray doubleList = null;
                if(data.containsKey(category)){
                     doubleList = data.getJSONArray(category);
                }else{
                     doubleList = new JSONArray();
                     data.put(category,doubleList);
                }
                boolean found = false;
                for(WeekBean weekBean:list){
                    if(weekBean.getLargeCategory().equals(category)){
                        doubleList.add(weekBean.getDuration());
                        sum += weekBean.getDuration();
                        found = true;
                    }
                }
                if(!found){
                    if(!"总时间".equals(category)){
                        doubleList.add(0.0);
                    }
                }
                //System.out.println("doubleList:"+doubleList);
            }
            if(sum>0){
                JSONArray sumList = data.getJSONArray("总时间");
                //把sum只保留2位小数点
                sum = Math.round(sum*100)/100.0;
                sumList.add(sum);
            }
            System.out.println("data:"+data);
        }

        return Result.success(res);
    }

    @GetMapping("/listOld")
    public Result<?> weekstatlistOld(@RequestParam(required = true) String username) {
        List<WeekBean> weekList = logMapper.getWeekList(username);
        TreeSet<String> weekSet = new TreeSet<>();
        TreeSet<String> categorySet = new TreeSet<>();

        HashMap<String, List<Double> > dataMap = new HashMap<>();

        for(WeekBean weekBean:weekList){
            String year = weekBean.getYear();
            String weekNumber = weekBean.getWeekNumber();
            weekSet.add(year+"-"+weekNumber);

            categorySet.add(weekBean.getLargeCategory());

            String largeCategory = weekBean.getLargeCategory();
            if(dataMap.containsKey(largeCategory)){
                List<Double> list = dataMap.get(largeCategory);
                list.add(weekBean.getDuration());
                dataMap.put(largeCategory,list);
            }else{
                List<Double> list = new ArrayList<>();
                list.add(weekBean.getDuration());
                dataMap.put(largeCategory,list);
            }
        }

        List <String> weekListForRes = new ArrayList<>(weekSet);
        List <String> categoryListForRes = new ArrayList<>(categorySet);

//        Iterator<String> iterator = weekSet.iterator();
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }

        JSONObject res = new JSONObject();
        res.set("categories", categoryListForRes);
        res.set("weeks", weekListForRes);
        res.set("data", dataMap);

        /**
         * {
         *   "categories": ["大类A", "大类B", ...],
         *   "weeks": ["2024-01", "2024-02", ..., "2024-30"],
         *   "data": {
         *     "大类A": [value1, value2, ..., value30],
         *     "大类B": [value1, value2, ..., value30],
         *     ...
         *   }
         * }
         */
        return Result.success(res);
    }




}
