package com.mvp.controller;


import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mvp.common.Result;
import com.mvp.entity.SubscriptionItem;
import com.mvp.mapper.LogMapper;
import com.mvp.mapper.SubscriptionItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

@RestController
@RequestMapping("/circleTarget")
public class circleTargetController {

    private static final Logger log = LoggerFactory.getLogger(circleTargetController.class);
    @Resource
    SubscriptionItemMapper subscriptionItemMapper;

    @Resource
    private LogMapper logMapper;


    @GetMapping("/getData")
    public Result<?> getData(@RequestParam(defaultValue = "") String username,
                             @RequestParam(defaultValue = "") String dataType) {
        System.out.println("------getData-----");
        JSONObject res = new JSONObject();
        JSONArray dataArr = new JSONArray();

        LocalDate today = LocalDate.now();
        LocalDate lastCircleStartDay;
        LocalDate lastCircleEndDay;
        LocalDate curCircleStartDay;
        LocalDate curCircleEndDay;

        // 确定各周期起止日期
        if ("day".equals(dataType)) {
            curCircleStartDay = today;
            curCircleEndDay = today;
            lastCircleStartDay = today.minusDays(1);
            lastCircleEndDay = today.minusDays(1);
        } else if ("week".equals(dataType)) {
            curCircleStartDay = today.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
            curCircleEndDay = curCircleStartDay.plusDays(6);
            lastCircleStartDay = curCircleStartDay.minusWeeks(1);
            lastCircleEndDay = lastCircleStartDay.plusDays(6);
        } else if ("month".equals(dataType)) {
            YearMonth currentMonth = YearMonth.from(today);
            curCircleStartDay = currentMonth.atDay(1);
            curCircleEndDay = currentMonth.atEndOfMonth();
            YearMonth lastMonth = currentMonth.minusMonths(1);
            lastCircleStartDay = lastMonth.atDay(1);
            lastCircleEndDay = lastMonth.atEndOfMonth();
        } else if ("quarter".equals(dataType)) {
            int currentQuarter = (today.getMonthValue() - 1) / 3 + 1;
            int currentYear = today.getYear();
            int startMonth = (currentQuarter - 1) * 3 + 1;
            curCircleStartDay = LocalDate.of(currentYear, startMonth, 1);
            curCircleEndDay = curCircleStartDay.plusMonths(3).minusDays(1);

            int lastQuarter = currentQuarter - 1;
            int lastYear = currentYear;
            if (lastQuarter == 0) {
                lastQuarter = 4;
                lastYear--;
            }
            startMonth = (lastQuarter - 1) * 3 + 1;
            lastCircleStartDay = LocalDate.of(lastYear, startMonth, 1);
            lastCircleEndDay = lastCircleStartDay.plusMonths(3).minusDays(1);
        } else if ("year".equals(dataType)) {
            curCircleStartDay = LocalDate.of(today.getYear(), 1, 1);
            curCircleEndDay = LocalDate.of(today.getYear(), 12, 31);
            lastCircleStartDay = LocalDate.of(today.getYear() - 1, 1, 1);
            lastCircleEndDay = LocalDate.of(today.getYear() - 1, 12, 31);
        } else {
            return Result.error("-1", "dataType 参数错误");
        }

        // 计算期望进度
        double expectedProgress = 0.0;
        switch (dataType) {
            case "day":
                LocalTime nowTime = LocalTime.now();
                LocalTime startWork = LocalTime.of(10, 0);
                LocalTime endWork = LocalTime.of(22, 0);
                if (nowTime.isBefore(startWork)) {
                    expectedProgress = 0.0;
                } else if (nowTime.isAfter(endWork) || nowTime.equals(endWork)) {
                    expectedProgress = 1.0;
                } else {
                    long totalMinutes = Duration.between(startWork, endWork).toMinutes();
                    long passedMinutes = Duration.between(startWork, nowTime).toMinutes();
                    expectedProgress = (double) passedMinutes / totalMinutes;
                }
                break;
            case "week":
                int dayOfWeek = today.getDayOfWeek().getValue(); // 1 (Monday) to 7 (Sunday)
                expectedProgress = dayOfWeek / 7.0;
                break;
            case "month":
                int totalDaysInMonth = YearMonth.from(today).lengthOfMonth();
                int dayOfMonth = today.getDayOfMonth();
                expectedProgress = (double) dayOfMonth / totalDaysInMonth;
                break;
            case "quarter":
                long totalQuarterDays = ChronoUnit.DAYS.between(curCircleStartDay, curCircleEndDay) + 1;
                long daysPassedInQuarter = ChronoUnit.DAYS.between(curCircleStartDay, today) + 1;
                expectedProgress = Math.min(1.0, Math.max(0.0, (double) daysPassedInQuarter / totalQuarterDays));
                break;
            case "year":
                int dayOfYear = today.getDayOfYear();
                int totalYearDays = today.isLeapYear() ? 366 : 365;
                expectedProgress = (double) dayOfYear / totalYearDays;
                break;
            default:
                expectedProgress = 0.0;
        }

        // 处理数据逻辑
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        queryWrapper.eq("subscription_time", dataType);
        queryWrapper.orderByDesc("sort_weight");
        List<SubscriptionItem> subscriptionItems = subscriptionItemMapper.selectList(queryWrapper);

        int CircleTarget = 0;
        int thisCircleActual = 0;
        int lastCircleActual = 0;
        for (SubscriptionItem subscriptionItem : subscriptionItems) {
            String subscriptionType = subscriptionItem.getSubscriptionType();
            String name = subscriptionItem.getName();
            Integer targetTime = subscriptionItem.getTargetTime();
            CircleTarget += targetTime;

            Integer curSum = getData(username, subscriptionType, name, curCircleStartDay, curCircleEndDay);
            thisCircleActual += Math.min(curSum, targetTime);

            Integer lastSum = getData(username, subscriptionType, name, lastCircleStartDay, lastCircleEndDay);
            lastCircleActual += Math.min(lastSum, targetTime);

            JSONObject obj = new JSONObject();
            obj.set("name", name);
            obj.set("curSum", curSum);
            obj.set("lastSum", lastSum);
            obj.set("thisCircleFinishRate", curSum * 1.0 / targetTime);
            obj.set("lastCircleFinishRate", lastSum * 1.0 / targetTime);
            obj.set("subscriptionType", subscriptionType);
            obj.set("target", targetTime);
            obj.set("expectedProgress", expectedProgress);  // 直接复用全局计算的进度比例
            dataArr.put(obj);
        }

        // 计算总完成率
        double lastCircleFinishRate = 1.0 * lastCircleActual / CircleTarget;
        double thisCircleFinishRate = 1.0 * thisCircleActual / CircleTarget;

        // 处理NaN
        if (Double.isNaN(lastCircleFinishRate)) lastCircleFinishRate = 0.0;
        if (Double.isNaN(thisCircleFinishRate)) thisCircleFinishRate = 0.0;

        // 构建返回结果
        res.put("curCircleStartDay", curCircleStartDay);
        res.put("curCircleEndDay", curCircleEndDay);
        res.put("lastCircleStartDay", lastCircleStartDay);
        res.put("lastCircleEndDay", lastCircleEndDay);
        res.put("lastCircleFinishRate", lastCircleFinishRate);
        res.put("thisCircleFinishRate", thisCircleFinishRate);
        res.put("CircleTarget", CircleTarget);
        res.put("dataArr", dataArr);
        res.put("expectedProgress", expectedProgress);

        return Result.success(res);
    }


    public Integer getData(String username,String subscriptionType,String name,LocalDate startDate,LocalDate endDate) {
        Integer res = 0;
        if("largeCategory".equals(subscriptionType)){
            Integer sumLargeCategory = logMapper.getSumLargeCategory(username, name, startDate, endDate);
            res = sumLargeCategory;
        }else if("type".equals(subscriptionType)){
            Integer sumType = logMapper.getSumType(username, name, startDate, endDate);
            res = sumType;
        }else if("project".equals(subscriptionType)){
            Integer sumProject = logMapper.getSumProject(username, name, startDate, endDate);
            res = sumProject;
        }else{
            res = 0;
            System.out.println("subscriptionType参数错误!");
        }
        return res;
    }
}
