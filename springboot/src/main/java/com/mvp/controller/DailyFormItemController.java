package com.mvp.controller;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mvp.common.Result;
import com.mvp.entity.DailyForm;
import com.mvp.entity.DailyFormItem;
import com.mvp.mapper.DailyFormItemMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
@RestController
@RequestMapping("/dailyFormItem")
public class DailyFormItemController {

    @Resource
    private DailyFormItemMapper dailyFormItemMapper;

    @GetMapping
    public Result<?> getAllDailyFormItem( @RequestParam(defaultValue = "") String date,
                             @RequestParam(defaultValue = "") String username) throws ParseException {
        System.out.println("----------getAllDailyFormItem-------------");
        System.out.println("date:"+date);
        System.out.println("username:"+username);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        Date day = new Date();
        if(StringUtils.isNotBlank(date)){
            day = sdf.parse(date);
        }
        LambdaQueryWrapper<DailyFormItem> wrapper = Wrappers.<DailyFormItem>lambdaQuery();

        if(StringUtils.isNotBlank(username)){
            wrapper.eq(DailyFormItem::getUsername, username);
        }else{
            wrapper.eq(DailyFormItem::getUsername, "unknown");
            System.out.println("用户名未知");
        }

        System.out.println("比较时间："+day);
        String format = sdf.format(day);
        String begin = format+" 00:00:00";
        String end = format+" 23:59:59";
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date beginTime = sdf2.parse(begin);
        Date endTime = sdf2.parse(end);
        wrapper.between(DailyFormItem::getDateTime, beginTime,endTime);

        wrapper.last("and (deleted = 0 or deleted is null) ");
//        wrapper.last("ORDER BY order_num DESC");

        List<DailyFormItem> logList = dailyFormItemMapper.selectList(wrapper);

        return Result.success(logList);
    }


    @PostMapping("/updateForm")
    public Result<?> updateForm( @RequestBody JSONObject obj){
        System.out.println("----------updateForm-------------");
        System.out.println(obj);
        String username = obj.getStr("username");
        JSONArray array = obj.getJSONArray("data");
        System.out.println("array:"+array);
        System.out.println("username:"+username);

        if(array!=null){
            for (int i = 0; i < array.size(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                System.out.print("jsonObject:"+jsonObject);
                String type = jsonObject.getStr("type");
                String value = jsonObject.getStr("value");
                Integer id = jsonObject.getInt("id");
                System.out.print("type"+type);
                System.out.print("value"+value);
                System.out.print("id"+id);
                if("数值".equals(type)){
                    String dataValue = value;
                    System.out.print("数值"+dataValue);
                    dailyFormItemMapper.updateDataValueById(value,dataValue,id);
                }else if("时间戳".equals(type)){
                    String dataTime = value;
                    System.out.print("时间戳"+dataTime);
                    dailyFormItemMapper.updateDateTimeById(value,dataTime,id);
                }else if("布尔".equals(type)){
                    String booleanValue = value;
                    System.out.print("布尔"+booleanValue);
                    dailyFormItemMapper.updateBooleanById(value,booleanValue,id);
                }else if("字符串".equals(type)){
                    String content = value;
                    System.out.print("字符串"+content);
                    dailyFormItemMapper.updateStringById(value,content,id);
                }else if("选项".equals(type)){
                    String selected = value;
                    System.out.print("选项"+selected);
                    dailyFormItemMapper.updateSelectById(value,selected,id);
                }else{
                    System.out.println("未知类型！");
                }
                System.out.println();
            }
        }
/**
 *     @Select("update daily_form_item set value = #{value},content = #{content} where id = #{id}")
 *     public Integer updateStringById(String value,String content,Integer id);
 *     @Select("update daily_form_item set value = #{value},data_value = #{dataValue} where id = #{id}")
 *     public Integer updateDataValueById(String value,String dataValue,Integer id);
 *     @Select("update daily_form_item set value = #{value},data_time = #{DateTime} where id = #{id}")
 *     public Integer updateDateTimeById(String value,String DateTime,Integer id);
 *     @Select("update daily_form_item set value = #{value},boolean_value = #{value} where id = #{id}")
 *     public Integer updateStringById(String value,Integer id);
 *         @Select("update daily_form_item set value = #{value},selected = #{selected} where id = #{id}")
 *     public Integer updateSelectById(String value,String selected,Integer id);
 */

        return Result.success();
    }
}
