package com.mvp.controller;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mvp.common.PubFun;
import com.mvp.common.Result;
import com.mvp.entity.Log;
import com.mvp.entity.ValueBean;
import com.mvp.mapper.LogMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@RestController
@RequestMapping("/log")
public class LogController {

    @Resource
    private LogMapper logMapper;


    @GetMapping("/getTypeByUser")
    public Result<?> getTypeByUser(@RequestParam(defaultValue = "") String username){
        System.out.println("------getTypeByUser-----");
        List<String> largeCategoryByUser = logMapper.getAllLargeCategory(username);
        List<String> typeByUser = logMapper.getTypeByUser(username);
        List<String> projectByUser = logMapper.getProjectByUser(username);
        JSONObject obj = new JSONObject();
        obj.set("typeByUser",typeByUser);
        obj.set("projectByUser",projectByUser);
        obj.set("largeCategoryByUser",largeCategoryByUser);
        return Result.success(obj);
    }

    @GetMapping("/getTypeByUserAndLargeCategory")
    public Result<?> getTypeByUserAndLargeCategory(
            @RequestParam(defaultValue = "") String username,
            @RequestParam(defaultValue = "") String largeCategory){
        System.out.println("------getTypeByUserAndLargeCategory-----");
        List<String> typeByUserAndLargeCategory = logMapper.getTypeByUserAndLargeCategory(username,largeCategory);
        return Result.success(typeByUserAndLargeCategory);
    }


    @GetMapping("/getProjectByUserAndType")
    public Result<?> getTypeByUser(@RequestParam(defaultValue = "") String username,@RequestParam(defaultValue = "") String type){
        System.out.println("------getProjectByUserAndType-----");
        List<String> projectByUserAndType = logMapper.getProjectByUserAndType(username,type);
        return Result.success(projectByUserAndType);
    }

    @PostMapping
    public Result<?> save(@RequestBody Log log){
        System.out.println("save log:"+log);
        Date dateTime = log.getDateTime();
        if (dateTime == null){
            dateTime = new Date();
            log.setDateTime(dateTime);
        }
        try{
            String dayOfWeek = getDayOfWeek(log.getDateTime());
            log.setDayOfWeek(dayOfWeek);
        }catch(Exception e){
            return Result.error("-1","请选择时间");
        }
        logMapper.insert(log);
        return Result.success();
    }

    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Long id){
        logMapper.deleteById(id);
        return Result.success();
    }

    @PutMapping
    public Result<?> update(@RequestBody Log log){
        String dayOfWeek = getDayOfWeek(log.getDateTime());
        log.setDayOfWeek(dayOfWeek);
        logMapper.updateById(log);
        return Result.success();
    }

    @GetMapping
    public Result<?> findPage(
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "") String date,
            @RequestParam(defaultValue = "") String username
            ) throws ParseException {
        //打印参数
        System.out.println("findPage-----");
        System.out.println("pageNum:"+pageNum);
        System.out.println("pageSize:"+pageSize);
        System.out.println("search:"+search);
        System.out.println("username:"+username);
        System.out.println("date:"+date);

        System.out.println("date:"+date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        Date day = new Date();
        if(StringUtils.isNotBlank(date)){
            day = sdf.parse(date);
        }
        LambdaQueryWrapper<Log> wrapper = Wrappers.<Log>lambdaQuery();

        if(StringUtils.isNotBlank(username)){
            wrapper.eq(Log::getUser, username);
        }else{
            wrapper.eq(Log::getUser, "unknown");
            System.out.println("用户名未知");
        }

        if(StringUtils.isNotBlank(search)){
            wrapper.like(Log::getDetail, search);
        }
        System.out.println("比较时间："+day);
        String format = sdf.format(day);
        String begin = format+" 00:00:00";
        String end = format+" 23:59:59";
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date beginTime = sdf2.parse(begin);
        Date endTime = sdf2.parse(end);
        wrapper.between(Log::getDateTime, beginTime,endTime);

        Page<Log> logPage = logMapper.selectPage(new Page<>(pageNum, pageSize),wrapper );
        List<Log> records = logPage.getRecords();
        for (Log log:records){
            Integer timing = log.getTiming();
            if(timing==1){
                log.setScore(1.0*log.getValue()*log.getDuration()*log.getUrgency()*log.getDifficulty()*log.getFocus()/60);
            }else{
                log.setScore(1.0*log.getValue()*log.getDuration()*log.getUrgency()*log.getDifficulty()*log.getFocus()/120);
            }
        }
        return Result.success(logPage);
    }

    @GetMapping("/statistic")
    public Result<?> getStatic(@RequestParam(defaultValue = "") String username, @RequestParam(defaultValue = "") String date) throws ParseException {
        System.out.println("date:"+date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        Date day = new Date();
        if(StringUtils.isNotBlank(date)){
            day = sdf.parse(date);
        }
        List<Log> logs = logMapper.selectStaticByUserAndDate(date, username);

        System.out.println("---------------");
        System.out.println("logs:"+logs);
        System.out.println("--------------");

        int sumOfWork = 0;

        JSONObject res = new JSONObject();
        int sum = 0;
        if(logs.size()!=0){
            for(Log log:logs){
                sum+=log.getDuration();
                System.out.println(log.getLargeCategory());
                if("工作".equals(log.getLargeCategory())){
                    sumOfWork+=log.getDuration();
                }
            }
        }

        //得到过去30天的价值
        //得到过去7天的价值
        int numOfValueDaysA = 30;
        int numOfValueDaysB = 7;
        String beginDayA = PubFun.getDayStrByFormatterAndMinusDay("yyyy-MM-dd",numOfValueDaysA-1);
        String beginDayB = PubFun.getDayStrByFormatterAndMinusDay("yyyy-MM-dd",numOfValueDaysB-1);

        System.out.println("beginDayA:"+beginDayA);
        System.out.println("beginDayA:"+beginDayA);
        List<ValueBean> valueBeansA = logMapper.selectEveryDayValueByUser(beginDayA, username);
        List<ValueBean> valueBeansB = logMapper.selectEveryDayValueByUser(beginDayB, username);

        Double sumValueA = 0.0;
        Double sumValueB = 0.0;
        for (ValueBean valueBean:valueBeansA){
            sumValueA+=valueBean.getValue();
        }
        for (ValueBean valueBean:valueBeansB){
            sumValueB+=valueBean.getValue();
        }
        Double avgValueA = sumValueA/numOfValueDaysA;
        Double avgValueB = sumValueB/numOfValueDaysB;

        //得到过去30天的总时间
        Double sumTimeA = logMapper.selectSumTimeByUserAndBeginDate(beginDayA, username);
        //得到过去7天的总时间
        Double sumTimeB = logMapper.selectSumTimeByUserAndBeginDate(beginDayB, username);

        res.set("sum",sum);
        res.set("sumOfWork",sumOfWork);
        res.set("avgValueA",avgValueA);
        res.set("avgValueB",avgValueB);
        res.set("sumTimeA",sumTimeA);
        res.set("sumTimeB",sumTimeB);
        res.set("logs",logs);
        System.out.println("-------statistic.res-------");
        System.out.println(res);
        return Result.success(res);
    }



    public String getDayOfWeek(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        int i = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if(i==0){
            i=7;
        }
        return i+"";
    }



}
