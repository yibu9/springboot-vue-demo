package com.mvp.controller;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.mvp.common.PubFun;
import com.mvp.common.Result;
import com.mvp.entity.Log;
import com.mvp.mapper.LogMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@RequestMapping("/stat")
public class StatController {
    @Resource
    private LogMapper logMapper;


    @GetMapping("/value/{username}")
    public Result<?> getPersonalValues(@PathVariable String username){
        String lastTenDaysMinusDay = PubFun.getDayStrByFormatterAndMinusDay("yyyy-MM-dd",29);
        return Result.success(logMapper.selectEveryDayValueByUser(lastTenDaysMinusDay,username));
    }

    @GetMapping("/rankList")
    public Result<?> rankList() {
        // 得到记录了日志的用户
        List<String> allAliveUser = logMapper.getAllAliveUser();
        System.out.println("allAliveUser:" + allAliveUser);

        // 得到最后的10天
        List<String> lastTenDays = PubFun.getLastDaysByFormatter(10, "MM-dd");
        System.out.println("lastTenDays:" + lastTenDays);

        // 得到9天前的日期
        String lastTenDaysMinusDay = PubFun.getDayStrByFormatterAndMinusDay("yyyy-MM-dd", 9);
        System.out.println("lastTenDaysMinusDay:" + lastTenDaysMinusDay);

        // 为每个用户创建对象
        JSONArray arr = new JSONArray();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        // 为每个用户查询10天内的时间数据
        for (int i = 0; i < allAliveUser.size(); i++) {
            JSONObject obj = new JSONObject();
            String username = allAliveUser.get(i);
            List<Log> logs = logMapper.selectLogsByUserAndDate(lastTenDaysMinusDay, username);

            // 按照日期排序,不够的全补0，构建一个大小为10的数组
            // 有的日期就是没有
            int[] values = new int[10];
            // 先初始化values,所有的值都变成0
            for (int j = 0; j < 10; j++) {
                values[j] = 0;
            }
            if (!logs.isEmpty()) {
                for (int cur = 0; cur < 10; cur++) {
                    String date = lastTenDays.get(cur);
                    for (int j = 0; j < logs.size(); j++) {
                        Log log = logs.get(j);
                        if ((sdf.format(log.getDateTime())).contains(date)) {
                            values[cur] = log.getDuration();
                            break;
                        }
                    }
                }
            }

            // 检查该用户的duration是否全部为0
            boolean allZero = true;
            for (int value : values) {
                if (value!= 0) {
                    allZero = false;
                    break;
                }
            }

            // 如果不是全部为0，则添加到结果数组中
            if (!allZero) {
                obj.put("name", username);
                obj.put("type", "line");

                // 显示label
                JSONObject label = new JSONObject();
                label.put("show", true);
                obj.put("label", label);

                // 本次新增2024年2月12日-平均线
                JSONObject dataObj = new JSONObject();
                dataObj.put("type", "average");
                dataObj.put("name", "Avg");
                JSONArray dataArr = new JSONArray();
                dataArr.add(dataObj);
                JSONObject markLine = new JSONObject();
                markLine.put("data", dataArr);
                obj.put("markLine", markLine);

                obj.put("data", values);
                arr.add(obj);
            }
        }

        JSONObject res = new JSONObject();
        res.put("allAliveUser", allAliveUser);
        res.put("lastTenDays", lastTenDays);
        res.put("lastTenDaysMinusDay", lastTenDaysMinusDay);
        res.put("arr", arr);

        return Result.success(res);
    }

    /**
     * 得到过去10天个人的数据统计信息
     * @return
     */
    @GetMapping("/{username}")
    public Result<?> rankList(@PathVariable String username){
        System.out.println("username:"+username);
        List<String> lastTenDays = PubFun.getLastDaysByFormatter(10,"MM-dd");
        //得到9天前的日期
        String lastTenDaysMinusDay = PubFun.getDayStrByFormatterAndMinusDay("yyyy-MM-dd",9);
        List<String> allAliveType = logMapper.getAllTypeByUserAndDate(lastTenDaysMinusDay, username);

        List<Log> logs = logMapper.selectLogsByUserAndDateAndType(lastTenDaysMinusDay,username);
        JSONArray arr = new JSONArray();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < allAliveType.size(); i++) {
            JSONObject obj = new JSONObject();
            String type = allAliveType.get(i);
            /**
             *{ name: 'Affiliate Ad', type: 'bar', stack: 'total', label: {show: true},
             * data: [220, 182, 191, 234, 290, 330, 310]
             * },
             */
            obj.set("name",type);
            obj.set("type","bar");
            obj.put("stack","total");
            JSONObject label  = new JSONObject();
            label.put("show",true);
            obj.put("label",label);
            int[] values = new int[10];
            for (int j = 0; j < 10; j++) {
                values[j] = 0;
            }
            if(!logs.isEmpty()){
                for( int cur = 0;cur<10;cur++){
                    String date = lastTenDays.get(cur);
                    for (int j = 0; j < logs.size(); j++) {
                        Log log = logs.get(j);
                        if(type.equals(log.getType()) && (sdf.format(log.getDateTime())).contains(date)){
                            values[cur] = log.getDuration();
                            break;
                        }
                    }
                }
            }
            obj.put("data",values);
            arr.add(obj);
        }

        JSONObject res = new JSONObject();
        res.put("allAliveType",allAliveType);
        res.put("lastTenDays",lastTenDays);
        res.put("lastTenDaysMinusDay",lastTenDaysMinusDay);
        res.put("arr",arr);
        return Result.success(res);
    }


}
