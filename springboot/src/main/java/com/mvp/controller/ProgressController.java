package com.mvp.controller;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mvp.common.Result;
import com.mvp.entity.PageResult;
import com.mvp.entity.Progress;
import com.mvp.entity.ProgressItem;
import com.mvp.entity.dto.ProgressDTO;
import com.mvp.mapper.ProgressItemMapper;
import com.mvp.mapper.ProgressMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

@RestController
@RequestMapping("/progress")
public class ProgressController {

    @Resource
    private ProgressMapper progressMapper;
    @Resource
    private ProgressItemMapper progressItemMapper;

    // 创建新的进度条目
    @PostMapping
    public Result<?> createProgress(@RequestBody ProgressDTO progressDTO) {
        System.out.println("------createProgress-----");
        System.out.println(progressDTO);
        String username = progressDTO.getUser();

        //先配置一个顺序
        int order = 0;
        Integer maxOrderByUser = progressMapper.getMaxOrderByUser(username);
        if(maxOrderByUser != null){
            order = maxOrderByUser + 10;
        }


        Progress progress = new Progress();
        //得到时间
        OffsetDateTime expectedCompletionTime = progressDTO.getExpectedCompletionTime();
        if(expectedCompletionTime != null){
            LocalDateTime localDateTime = expectedCompletionTime.toLocalDateTime();
            progress.setExpectedCompletionTime(localDateTime);
        }
        progress.setUser(username);
        progress.setName(progressDTO.getName());
        progress.setTargetValue(progressDTO.getTargetValue());
        progress.setOrderNum(order);
        progress.setContent(progressDTO.getContent());
        progress.setUnit(progressDTO.getUnit());
        progress.setStatus(progressDTO.getStatus());
        progress.setEvaluation(progressDTO.getEvaluation());
        progress.setCurrentValue(0.0);
        progress.setType(progressDTO.getType());
        progress.setCreateTime(LocalDateTime.now());
        progress.setPercentage(0.0);
        boolean result = progressMapper.insert(progress) > 0;
        return result ? Result.success() : Result.error("-1", "创建失败");
    }

    // 获取所有进度条目
    @GetMapping
    public Result<?> getAllProgress(
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "100") Integer pageSize,
            @RequestParam(defaultValue = "") String user,
            @RequestParam(defaultValue = "") String name,
            @RequestParam(defaultValue = "") String type
    ) {
        System.out.println("------getAllProgress-----");
        System.out.println("user:"+user);
        System.out.println("name:"+name);
        System.out.println("type:"+type);
        System.out.println("pageNum:"+pageNum);
        System.out.println("pageSize:"+pageSize);

// 创建主查询条件
        LambdaQueryWrapper<Progress> wrapper = Wrappers.<Progress>lambdaQuery();

        if (StringUtils.isBlank(user)) {
            return Result.error("-1", "用户名不能为空！");
        }

        if (StringUtils.isNotBlank(user)) {
            wrapper.eq(Progress::getUser, user);
        }

// 定义 deleted 相关的子条件链，并确保它们与 user 条件是 AND 关系
        wrapper.and(subWrapper -> subWrapper
                .ne(Progress::getDeleted, 1)
                .or()
                .isNull(Progress::getDeleted)
        );

        if (StringUtils.isNotBlank(type)) {
            if ("未分类".equals(type)) {
                wrapper.isNull(Progress::getType);
            } else {
                wrapper.eq(Progress::getType, type);
            }
        }

        if (StringUtils.isNotBlank(name)) {
            wrapper.like(Progress::getName, name);
        }

        wrapper.orderByDesc(Progress::getOrderNum);

        Page<Progress> page = new Page<>(pageNum, pageSize);
        Page<Progress> progressPage = progressMapper.selectPage(page, wrapper);

        List<String> typeListByUser = progressMapper.getTypeListByUser(user);
        JSONArray types = makeTypeArr(typeListByUser);

        JSONObject res = new JSONObject();
        res.set("types", types);
        res.set("data", progressPage);
        return Result.success(res);
    }

    public JSONArray makeTypeArr(List<String> type){
        JSONArray typeArr = new JSONArray();
        if(type == null || type.size() == 0){
            return typeArr;
        }
        HashSet<String> set = new HashSet<>();
        for(String s : type){
            if(StringUtils.isNotBlank(s)){
                set.add(s);
            }
        }
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            String curType = iterator.next();
            if(StringUtils.isNotBlank(curType)){
                JSONObject obj = new JSONObject();
                obj.set("value", curType);
                obj.set("label", curType);
                typeArr.add(obj);
            }
        }
        JSONObject obj = new JSONObject();
        obj.set("value", "未分类");
        obj.set("label", "未分类");
        typeArr.add(obj);
        return typeArr;
    }

    // 根据ID获取单个进度条目
    @GetMapping("/{id}")
    public Result<?> getProgressById(@PathVariable Integer id) {
        System.out.println("------getProgressById-----");
        Progress progress = progressMapper.selectById(id);
        if (progress == null) {
            return Result.error("-1", "未找到该进度条目");
        }
        return Result.success(progress);
    }

    // 更新进度条目
    @PutMapping
    public Result<?> updateProgress( @RequestBody Progress progress) {
        System.out.println("------updateProgress-----");
        System.out.println(progress);
        fillAdvice(progress);
        boolean result = progressMapper.updateById(progress) > 0;

        if(result){
            //驱动重新计算进度条
            calculateProgress(progress);
        }
        return result ? Result.success() : Result.error("-1", "更新失败");
    }

    public void calculateProgress(Progress progress){
        if(progress != null){
            List<ProgressItem> progressItems = progressItemMapper.selectProgressItemByProgressId(progress.getId());
            double curSum = 0 ;
            for (ProgressItem curProgressItem : progressItems) {
                curSum += curProgressItem.getValue();
            }
            double target = progress.getTargetValue();
            double percentage = 100.0* curSum / target;
            progress.setPercentage(percentage);
            progress.setCurrentValue(curSum);
            progressMapper.updateByIdSelective(progress);
        }
    }

    @PostMapping("/addItem")
    public Result<?> updateProgress( @RequestBody ProgressItem item) {
        System.out.println("------addItem-----");
        System.out.println(item);

        //校验一下看看，值是否都ok
        if(item.getValue() == null ){
            return Result.error("-1","value不能为空");
        }
        if(item.getProgressId() == null || item.getProgressId() <= 0){
            return Result.error("-1","progressId不能为空，且必须大于0");
        }
        if(item.getDateTime() == null){
            return Result.error("-1","dateTime不能为空");
        }
        Progress progress = progressMapper.selectById(item.getProgressId());
        int insert = progressItemMapper.insert(item);
        if(insert < 1){
            return Result.error("-1","新增失败");
        }

        List<ProgressItem> progressItems = progressItemMapper.selectProgressItemByProgressId(item.getProgressId());
        double curSum = 0 ;
        for (ProgressItem progressItem : progressItems) {
            curSum += progressItem.getValue();
        }
        double target = progress.getTargetValue();
        double percentage = 100.0* curSum / target;
        progress.setPercentage(percentage);
        progress.setCurrentValue(curSum);
        fillAdvice(progress);
        int i = progressMapper.updateById(progress);
        boolean result = i>0;
        return result ? Result.success() : Result.error("-1", "新增失败");
    }


    @GetMapping("/items")
    public PageResult<ProgressItem> getProgressItemsByProgressId(
            @RequestParam(required = true)Integer progressId,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer size) {
        System.out.println("------getProgressItemsByProgressId-----");
        System.out.println("progressId:"+progressId);
        System.out.println("page:"+page);
        System.out.println("size:"+size);
        // 计算数据库查询的起始位置
        int offset = (page - 1) * size;
        // 查询总条目数
        long total = progressItemMapper.countByProgressId(progressId);
        // 查询分页数据
        List<ProgressItem> items = progressItemMapper.selectByProgressId(progressId, offset, size);
        // 封装分页结果
        return new PageResult<>(items, total,0);
    }

    @DeleteMapping("/deleteItem")
    public Result<?> deleteItem( @RequestParam(required = true) Integer id) {
        System.out.println("------deleteItem-----");
        System.out.println(id);

        if(id == null ){
            return Result.error("-1","id不能为空");
        }
        ProgressItem progressItem = progressItemMapper.selectById(id);
        if(progressItem == null){
            return Result.error("-1","id不存在");
        }

        Progress progress = progressMapper.selectById(progressItem.getProgressId());
        int insert = progressItemMapper.deleteById(id);
        if(insert < 1){
            return Result.error("-1","删除失败");
        }

        List<ProgressItem> progressItems = progressItemMapper.selectProgressItemByProgressId(progressItem.getProgressId());
        double curSum = 0 ;
        for (ProgressItem curProgressItem : progressItems) {
            curSum += curProgressItem.getValue();
        }
        double target = progress.getTargetValue();
        double percentage = 100.0* curSum / target;
        progress.setPercentage(percentage);
        progress.setCurrentValue(curSum);
        fillAdvice(progress);
        int i = progressMapper.updateById(progress);
        boolean result = i>0;
        return result ? Result.success() : Result.error("-1", "新增失败");
    }


    @GetMapping("/changeOrder")
    public Result<?> changeOrder(
            @RequestParam(required = true)Integer progressId,
            @RequestParam(required = true) String  user,
            @RequestParam(required = true) String direction) {
        System.out.println("------changeOrder-----");
        System.out.println("progressId:"+progressId);
        System.out.println("user:"+user);
        System.out.println("direction:"+direction);
        if(StringUtils.isBlank(user)){
            return Result.error("-1","user不能为空");
        }
        if(StringUtils.isBlank(direction)){
            return Result.error("-1","direction不能为空");
        }
        if(!direction.equals("up") && !direction.equals("down")){
            return Result.error("-1","direction只能为up或down");
        }

        Progress progress = progressMapper.selectById(progressId);
        if(progress == null){
            return Result.error("-1","progressId不存在");
        }

        Integer orderNum = progress.getOrderNum(); //得到了当前的排序号
        if("up".equals(direction)){
            //需要找到比大的排序号中的最小的一个，和他交换顺序
            Progress progressAboveThis = progressMapper.selectMinOrderNumProgressAboveThisByUser(user, orderNum);
            if(progressAboveThis == null){
                return Result.error("1","没有比当前排序号更大的");
            }
            //交换他们的orderNum
            progress.setOrderNum(progressAboveThis.getOrderNum());
            progressAboveThis.setOrderNum(orderNum);
            progressMapper.updateById(progress);
            progressMapper.updateById(progressAboveThis);
        }else if ("down".equals(direction)){
            Progress progressUnderThis = progressMapper.selectMaxOrderNumProgressUnderThisByUser(user, orderNum);
            if(progressUnderThis == null){
                return Result.error("2","没有比当前排序号更小的");
            }
            //需要找到比他小的排序号中的最大的一个，和他交换顺序
            progress.setOrderNum(progressUnderThis.getOrderNum());
            progressUnderThis.setOrderNum(orderNum);
            progressMapper.updateById(progress);
            progressMapper.updateById(progressUnderThis);
        }else{
            return Result.error("-1","direction只能为up或down");
        }

        return Result.success();
    }


    @GetMapping("/chart")
    public Result<?> chart(
            @RequestParam(required = true)Integer progressId,
            @RequestParam(required = true) String  user) {
        //先验证user和progressId是否合法
        if(StringUtils.isBlank(user)){
            return Result.error("-1","user不能为空");
        }
        if(progressId == null){
            return Result.error("-1","progressId不能为空");
        }

        List<ProgressItem> progressItems = progressItemMapper.selectProgressItemByProgressId(progressId);
        Progress progress = progressMapper.selectById(progressId);
        LocalDateTime createTime = progress.getCreateTime();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expectedCompletionTime = progress.getExpectedCompletionTime();
        boolean hasExpectedCompletionTime = expectedCompletionTime != null;

        System.out.println("创建时间："+createTime);
        System.out.println("现在："+now);
        System.out.println("ddl："+expectedCompletionTime);
        System.out.println("是否有ddl"+hasExpectedCompletionTime);

        //计算now和createTime之间的天数
        long betweenDaysFromNow = (int) ChronoUnit.DAYS.between(createTime, now) +1;
        System.out.println("betweenDaysFromNow:"+betweenDaysFromNow);

        JSONArray series = new JSONArray();

        //实际曲线
        JSONArray actualDataJSONArray = new JSONArray();
        JSONObject actual = new JSONObject();
        //从createTime一直遍历到今天，每天增加一个点，点的值是当天的progressItem的值
        Double value = 0.0;
        LocalDateTime beforeStartDate = createTime.minusDays(1);
        String beforeStartDateStr = beforeStartDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        actualDataJSONArray.put(new Object[]{beforeStartDateStr,0.0});
        for (int i = 0; i <= betweenDaysFromNow; i++) {
            LocalDateTime date = createTime.plusDays(i);
            String dateStr = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            //判断dateStr是否在progressItems中，如果在，则取progressItem的值，否则取0
            for (ProgressItem progressItem : progressItems) {
                //如果日期相等，则取该progressItem的值
                if(dateStr.equals(progressItem.getDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))){
                    value+= progressItem.getValue();
                }
            }
            actualDataJSONArray.put(new Object[]{dateStr,value});
        }
        actual.put("name","实际");
        actual.put("symbol","none");
        actual.put("smooth",false);
        actual.put("line", "总量");
        actual.put("type","line");
        System.out.println("actual-data"+actualDataJSONArray);
        actual.put("data",actualDataJSONArray);
        series.put(actual);

        if(hasExpectedCompletionTime){
            //计算 expectedCompletionTime和createTime之间的天数
            long betweenDaysFromDDL = (int) ChronoUnit.DAYS.between(createTime, expectedCompletionTime) +1;
            Double targetValue = progress.getTargetValue();
            Double everyDayValue = targetValue / betweenDaysFromDDL;
            System.out.println("截止日期和起始日期之间有："+betweenDaysFromDDL+"天");
            System.out.println("目标值是："+targetValue);
            System.out.println("每天目标值是："+everyDayValue);

            Double standardValue = 0.0;

            //现在只针对匀速的直线
            JSONArray standardDataJSONArray = new JSONArray();
            JSONObject standard = new JSONObject();
            standardDataJSONArray.put(new Object[]{beforeStartDateStr,0.0});
            for (int i = 0; i < betweenDaysFromDDL; i++) {
                LocalDateTime date = createTime.plusDays(i);
                String dateStr = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                //判断dateStr是否在progressItems中，如果在，则取progressItem的值，否则取0
                standardValue = i*everyDayValue + everyDayValue;

                standardDataJSONArray.put(new Object[]{dateStr,standardValue});
            }
            standard.put("name","期望");
            standard.put("symbol","none");
            standard.put("smooth",false);
            standard.put("line", "总量");
            standard.put("type","line");
            System.out.println("standard-data"+standardDataJSONArray);
            standard.put("data",standardDataJSONArray);
            series.put(standard);
        }

        JSONObject opionsBySeries = getOpionsBySeries(series, progress.getUnit(), "日期");
        return Result.success(opionsBySeries);
    }

    public JSONObject getOpionsBySeries(JSONArray series, String yName, String xName){
        JSONObject legend = new JSONObject();
        legend.put("top","2%");
        legend.put("left","center");

        JSONObject grid = new JSONObject();
        grid.put("top","20%");
        grid.put("bottom","5%");
        grid.put("left","5%");
        grid.put("right","5%");
        grid.put("containLabel",true);

        JSONObject xAxis = new JSONObject();
        xAxis.put("type","time");
        if(!org.springframework.util.StringUtils.isEmpty(xName)){
            xAxis.put("name",xName);
        }
        JSONObject splitLine = new JSONObject();
        splitLine.put("show",false);
        xAxis.put("splitLine",splitLine);

        JSONObject yAxis = new JSONObject();
        yAxis.put("type","value");
        if(!org.springframework.util.StringUtils.isEmpty(yName)){
            yAxis.put("name",yName);
        }
        yAxis.put("boundaryGap",0);

        JSONObject options = new JSONObject();
        options.put("grid",grid);
        options.put("legend",legend);
        options.put("xAxis",xAxis);
        options.put("yAxis",yAxis);
        options.put("series",series);
        return options;
    }

    @GetMapping("/delete")
    public Result<?> delete(
            @RequestParam(required = true)String  progressId,
            @RequestParam(required = true) String  user) {
        //打印参数
        System.out.println("delete方法:");
        System.out.println("id:"+progressId);
        System.out.println("username:"+user);
        //校验参数
        if(StringUtils.isBlank(progressId)){
            return Result.error("-1","id不能为空");
        }
        if(StringUtils.isBlank(user)){
            return Result.error("-1","username不能为空");
        }
        //执行方法
        int result = progressMapper.deleteByIdAndUser(Integer.parseInt(progressId),user);
        return Result.success(result);
    }


    /**
     *  计算进度
     *  传入一个进度条对象
     *  会对对象现在应该有的进度进行计算（用创建时间和截止时间，得出当前应该有的进度）
     *  然后再结合当前进度和目标进度之间的差值，给出提示，完善statusMsg和advice
     *  statusMsg表示你现在的进度是落后正常还是领先
     *  advice给你出建议，让你知道你每天应该做多少单位的目标事项
     *  @param progress
     */
    public static void fillAdvice(Progress progress){
        System.out.println("============================");
        System.out.println(progress);
        if(progress != null && progress.getExpectedCompletionTime()!=null){
            Double targetValue = progress.getTargetValue();
            Double currentValue = progress.getCurrentValue();

            LocalDateTime createTime = progress.getCreateTime();
            LocalDateTime expectedCompletionTime = progress.getExpectedCompletionTime();
            LocalDateTime now = LocalDateTime.now();

            System.out.println("targetValue:"+targetValue);
            System.out.println("currentValue:"+currentValue);
            System.out.println("createTime:"+createTime);
            System.out.println("expectedCompletionTime:"+expectedCompletionTime);
            System.out.println("now:"+now);

            if(currentValue >= targetValue){
                System.out.println("当前进度大于等于目标进度，不需要再做,已完成");
                //todo 已完成
                //填充已完成的信息，返回即可
                progress.setStatusMsg("已完成");
                progress.setAdvice("");
                return;
            }
            //检查时间的逻辑
            if(now.isAfter(expectedCompletionTime)){
                System.out.println("截止日期已过，请修改");
                //todo 截止日期已过！
                //填充信息，返回即可
                progress.setStatusMsg("截止日期已过");
                progress.setAdvice("");
                return;
            }

            if(createTime.isAfter(expectedCompletionTime)){
                System.out.println("截止日期选择错误，请修改");
                //todo 截止日期选择错误！
                //填充信息，返回即可
                progress.setStatusMsg("截止日期选择错误，请修改");
                progress.setAdvice("");
                return;
            }

            //计算截止日期到起始日期之间有多少天
            long betweenDaysFromDDL = (int) ChronoUnit.DAYS.between(createTime, expectedCompletionTime) +1;
            //计算现在到起始日期之间有多少天
            long betweenDaysFromNow = (int) ChronoUnit.DAYS.between(createTime, now) +1;
            //计算现在到截止日期之间的天数
            long betweenDaysFromDDLandNow = (int) ChronoUnit.DAYS.between(now, expectedCompletionTime) +1;

            //计算理论上应该完成的百分比
            Double expectedPercentage = (double) betweenDaysFromNow/betweenDaysFromDDL*100;

            //检查当前进度和理论进度之间的差值
            Double percentage = progress.getPercentage();
            //当日当前进度减掉理论进度
            Double diff = percentage - expectedPercentage;

            System.out.println("计算截止日期到起始日期之间有多少天betweenDaysFromDDL:"+betweenDaysFromDDL);
            System.out.println("计算现在到起始日期之间有多少天betweenDaysFromNow:"+betweenDaysFromNow);
            System.out.println("计算现在到截止日期之间的天数betweenDaysFromDDLandNow:"+betweenDaysFromDDLandNow);
            System.out.println("计算理论上应该完成的百分比expectedPercentage:"+expectedPercentage);
            System.out.println("检查当前进度percentage:"+percentage);
            System.out.println("和理论进度之间的差值diff:"+diff);

            String msg = "";
            if(diff>15){
                msg = "进度大幅度领先";
            }else if(diff>5){
                msg = "进度领先";
            }else if(diff>0){
                msg = "进度正常";
            }else if(diff>-10){
                msg = "进度落后，请努力";
            }else if(diff<=-10){
                msg = "进度大幅度落后,还想按时完成吗？";
            }
            progress.setDiff(diff);
            progress.setStatusMsg(msg);
            System.out.println("msg:"+msg);
            //计算接下来每天需要做的
            double adviceEffort = (100-percentage)*0.01*targetValue/betweenDaysFromDDLandNow;
            System.out.println("(1-"+percentage+")*"+targetValue+"/"+betweenDaysFromDDLandNow+"="+adviceEffort);
            System.out.println("计算接下来每天需要做的adviceEffort:"+adviceEffort);
            //adviceEffort保留两位小数
            adviceEffort = (double) ((int)(adviceEffort*100))/100;
            progress.setExpectedValue(adviceEffort);
            progress.setAdvice("为按时完成，每天建议做："+adviceEffort+progress.getUnit());
            progress.setAdviceValue(adviceEffort);
            System.out.println(progress);
        }
    }


    // 获取所有进度条目
    @GetMapping("/stat")
    public Result<?> getStat(
            @RequestParam(defaultValue = "") String user
    ) {
        System.out.println("------getAllProgress-----");
        System.out.println("user:"+user);

        LambdaQueryWrapper<Progress> wrapper = Wrappers.<Progress>lambdaQuery();

        if (StringUtils.isBlank(user)) {
            return Result.error("-1", "用户名不能为空！");
        }

        if (StringUtils.isNotBlank(user)) {
            wrapper.eq(Progress::getUser, user);
        }

        // 定义 deleted 相关的子条件链，并确保它们与 user 条件是 AND 关系
        wrapper.and(subWrapper -> subWrapper
                .ne(Progress::getDeleted, 1)
                .or()
                .isNull(Progress::getDeleted)
        );

        wrapper.orderByDesc(Progress::getOrderNum);
        List<Progress> progressPage = progressMapper.selectList(wrapper);
        System.out.println("progressPage:"+progressPage);
        ArrayList<Progress> HourList = new ArrayList<>();
        ArrayList<Progress> MinList = new ArrayList<>();
        ArrayList<Progress> OtherList = new ArrayList<>();
        //遍历所有的progressPage
        for (Progress progress : progressPage) {
            String unit = progress.getUnit();
            if("小时".equals(unit) || "时".equals(unit) || "h".equals(unit) || "H".equals(unit) || "hours".equals(unit) || "Hours".equals(unit) || "hour".equals(unit)){
                System.out.println("unit!HourList:"+unit);
                System.out.println(progress);
                HourList.add(progress);
            }else if("分钟".equals(unit) || "分".equals(unit) || "m".equals(unit) || "M".equals(unit) || "minutes".equals(unit) || "Minutes".equals(unit) || "minute".equals(unit)|| "min".equals(unit)){
                MinList.add(progress);
                System.out.println("unit!MinList:"+unit);
                System.out.println(progress);
            }else{
                OtherList.add(progress);
                System.out.println("unit!OtherList:"+OtherList);
                System.out.println(progress);
            }
        }
        double sumHour = 0;
        System.out.println("HourList:"+HourList);
        for (Progress progress : HourList) {
            sumHour += progress.getAdviceValue();
        }
        double sumMin = 0;
        for (Progress progress : MinList) {
            if(progress.getAdviceValue()==null){
                sumMin+=0;
            }else{
                sumMin += progress.getAdviceValue();
            }
        }
        System.out.println("sumMin:"+sumMin);
        System.out.println("sumHour:"+sumHour);
        sumMin = sumMin + sumHour*60;
        Progress sumProgress = new Progress();
        sumProgress.setName("当日任务总计");
        sumProgress.setAdviceValue(sumMin);
        sumProgress.setUnit("分钟");
        OtherList.add(0,sumProgress);
        JSONObject res = new JSONObject();
        res.set("data", OtherList);
        return Result.success(res);
    }
}