package com.mvp.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.mvp.common.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

import static com.mvp.common.Result.success;

@RestController
@RequestMapping("/files")
public class FileController {

    @Value("${server.port}")
    private String port;

    private static final String ip = "http://localhost";


    /**
     * 上传借口
     * @param file
     * @return
     * @throws IOException
     */
    @CrossOrigin
    @PostMapping("/upload")
    public Result<?> upload(@RequestParam("file") MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();//获取源文件的名称
        //定义文件的唯一标识前缀
        String flag = IdUtil.fastSimpleUUID();
        String rootFilePath = System.getProperty("user.dir")+"/springboot/src/main/resources/files/"+flag+"_"+originalFilename;//获取上传路径
        FileUtil.writeBytes(file.getBytes(), rootFilePath);//把文件写入到上传的路径
        return success(ip+":"+port+"/files/"+flag);//返回结果url
    }

    /**
     * 下载文件
     * @param flag
     * @param response
     * @throws IOException
     */
    @GetMapping("/{flag}")
    public void getFiles(@PathVariable String flag, HttpServletResponse response) throws IOException {
        OutputStream os; //新建一个输出流对象
        String basePath = System.getProperty("user.dir")+"/springboot/src/main/resources/files/"; //定义文件上传的根路径
        List<String> fileNames = FileUtil.listFileNames(basePath);//获取所有的文件名称
        String fileName = fileNames.stream().filter(name -> name.contains(flag)).findAny().orElse("");//找到跟参数一直的文件
        try{
            if(StrUtil.isNotBlank(fileName)){
                response.addHeader("Content-Disposition", "attachment;filename="+ URLEncoder.encode(fileName, "UTF-8"));
                response.setContentType("application/octet-stream");
                byte[] bytes = FileUtil.readBytes(basePath + fileName); //通过文件的路径读取文件字节流
                os = response.getOutputStream();  //通过输出流返回文件
                os.write(bytes);
                os.flush();
                os.close();
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("文件下载失败");
        }
    }


    /**
     * 上传富文本接口
     * @param file
     * @return
     * @throws IOException
     */
    @CrossOrigin
    @PostMapping("/editor/upload")
    public JSONObject editorUpload(@RequestParam("file") MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();//获取源文件的名称
        //定义文件的唯一标识前缀
        String flag = IdUtil.fastSimpleUUID();
        String rootFilePath = System.getProperty("user.dir")+"/springboot/src/main/resources/files/"+flag+"_"+originalFilename;//获取上传路径
        FileUtil.writeBytes(file.getBytes(), rootFilePath);//把文件写入到上传的路径
        String url = ip+":"+port+"/files/"+flag;
        JSONObject obj = new JSONObject();
        obj.put("errno",0);
        JSONObject data = new JSONObject();
        JSONArray arr = new JSONArray();
        data.set("url",url);
        arr.add(data);
        obj.put("data",arr);
        return obj;
    }

}
