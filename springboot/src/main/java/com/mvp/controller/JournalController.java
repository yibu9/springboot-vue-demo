package com.mvp.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mvp.common.Result;
import com.mvp.entity.Journal;
import com.mvp.mapper.JournalMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/journal")
public class JournalController {

    @Resource
    private JournalMapper journalMapper;

    @PostMapping
    public Result<?> save(@RequestBody Journal journal){
        System.out.println("save journal:"+journal);
        System.out.println(journal.getDateTime());
        if(journal.getDateTime()==null){
            journal.setDateTime(new Date());
        }
        if(StringUtils.isBlank(journal.getUser())){
            return Result.error("-1","用户名不能为空");
        }
        if(StringUtils.isBlank(journal.getContent())){
            return Result.error("-1","内容不能为空");
        }

        journalMapper.insert(journal);
        return Result.success();
    }

    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Long id){
        journalMapper.deleteById(id);
        return Result.success();
    }

    @PutMapping
    public Result<?> update(@RequestBody Journal journal){
        journalMapper.updateById(journal);
        return Result.success();
    }

    @GetMapping
    public Result<?> findJournals(
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "") String date,
            @RequestParam(defaultValue = "") String username
            ) throws ParseException {
        System.out.println("findJournals-----");
        System.out.println("pageNum:"+pageNum);
        System.out.println("pageSize:"+pageSize);
        System.out.println("search:"+search);
        System.out.println("username:"+username);
        System.out.println("date:"+date);


        LambdaQueryWrapper<Journal> wrapper = Wrappers.<Journal>lambdaQuery();
        if(StringUtils.isNotBlank(username)){
            wrapper.eq(Journal::getUser, username);
        }else{
            wrapper.eq(Journal::getUser, "unknown");
            System.out.println("用户名未知");
        }

        if(StringUtils.isNotBlank(search)){

            String[] s = search.split(" ");
            for (String searchStr :s){
                wrapper.like(Journal::getContent, searchStr);
            }
        }

        if(StringUtils.isNotBlank(date) && !date.contains("T")){
            wrapper.eq(Journal::getDateTime, date);
        }
        wrapper.orderByDesc(Journal::getDateTime);
        wrapper.orderByDesc(Journal::getId);
        Page<Journal> journalPage = journalMapper.selectPage(new Page<>(pageNum, pageSize),wrapper );
        return Result.success(journalPage);
    }


    @GetMapping("/oneday")
    public Result<?> findOneDaysJournals(
            @RequestParam(defaultValue = "") String date,
            @RequestParam(defaultValue = "") String username
    ) throws ParseException {
        System.out.println("findOneDaysJournals-----");
        System.out.println("username:"+username);
        System.out.println("date:"+date);


        LambdaQueryWrapper<Journal> wrapper = Wrappers.<Journal>lambdaQuery();
        if(StringUtils.isNotBlank(username)){
            wrapper.eq(Journal::getUser, username);
        }else{
            wrapper.eq(Journal::getUser, "unknown");
            System.out.println("用户名未知");
        }
        wrapper.like(Journal::getDateTime, date);
        wrapper.orderByDesc(Journal::getDateTime);
        wrapper.orderByDesc(Journal::getId);

        List<Journal> journals = journalMapper.selectList(wrapper);
        return Result.success(journals);
    }
}
