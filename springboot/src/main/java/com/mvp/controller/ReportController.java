package com.mvp.controller;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.mvp.common.Result;
import com.mvp.entity.Log;
import com.mvp.mapper.LogMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/report")
public class ReportController {
    @Resource
    private LogMapper logMapper;

    @GetMapping("/getReport")
    public Result<?> getReport(
            @RequestParam(defaultValue = "")  String username,
            @RequestParam(defaultValue = "") String beginDate,
            @RequestParam(defaultValue = "")   String endDate,
            @RequestParam(defaultValue = "")   String method){
        System.out.println("getReport方法:");
        //打印参数
        System.out.println("username:"+username);
        System.out.println("beginDate:"+beginDate);
        System.out.println("endDate:"+endDate);
        System.out.println("method:"+method);



        if(StringUtils.isBlank(username) || StringUtils.isBlank(beginDate) || StringUtils.isBlank(endDate) || StringUtils.isBlank(method)){
            return Result.error("-1","参数不全,请检查");
        }

        List<Log> logs = logMapper.selectLogsByUserAndDateRange(username, beginDate, endDate);
        JSONObject SumRes = new JSONObject();
        //TreeMap<String,JSONObject> DaysRes  = new TreeMap();
        JSONArray DaysRes = new JSONArray();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if("sum".equals(method)){
            if(logs.size()==0){

            }else{
                SumRes = getStatObject(logs);
            }
        }else if("day".equals(method)){
            //得到所有的日期
            TreeSet<Date> dates = new TreeSet<>();
            for (Log log : logs) {
                dates.add(log.getDateTime());
            }

            //将查询到的日志按照日期分组
            HashMap<Date,List<Log>> map = new HashMap<>();
            for (Date date : dates) {
                List<Log> logsOfThisDay = new ArrayList<>();
                for(Log log : logs){
                    if(log.getDateTime().equals(date)){
                        logsOfThisDay.add(log);
                    }
                }
                map.put(date,logsOfThisDay);
            }
            //遍历map
            for (Date date : dates) {
                List<Log>  curLogs = map.get(date);
                JSONObject statObject = getStatObject(curLogs);

                JSONObject item = new JSONObject();
                item.put("key",sdf.format(date));
                item.put("value",statObject);
                DaysRes.add(item);

            }
        }

        if("sum".equals(method)){
            int sumOfTime = 0;
            JSONObject typeDuration = new JSONObject();
            JSONArray chartObject = new JSONArray();
            //遍历SumRes
            Set<String> strings = SumRes.keySet();
            for (String key : strings) {
                JSONArray jsonArray = SumRes.getJSONArray(key);
                //key是一个type,value是一个JSONArray
                int sum = 0;
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    sum += jsonObject.getInt("duration");
                    sumOfTime+= jsonObject.getInt("duration");
                }
                typeDuration.put(key,sum);
                JSONObject chartItem = new JSONObject();
                chartItem.put("name",key);
                chartItem.put("value",sum);
                chartObject.add(chartItem);
            }
            JSONObject resData = new JSONObject();
            resData.put("typeDuration",typeDuration);
            resData.put("sum",SumRes);
            resData.put("sumOfTime",sumOfTime);
            resData.put("chartObject",chartObject);
            return Result.success(resData);
        }else{
            //NavigableMap<String, JSONObject> stringJSONObjectNavigableMap = DaysRes.descendingMap();
            return Result.success(DaysRes);
        }


    }

    private JSONObject getStatObject(List<Log> logs){
        JSONObject res = new JSONObject();
        HashSet<String> types = new HashSet<>();
        for (Log log : logs) {
            types.add(log.getType());
        }
        //遍历types
        for (String type : types) {
            JSONArray arr = new JSONArray();
            //遍历所有的logs
            for (Log log : logs) {
                if (log.getType().equals(type)) {
                    String project = log.getProject();
                    Integer duration = log.getDuration();
                    String reportContent = log.getReportContent();

                    boolean alreadyExist = false;
                    //遍历arr
                    for (int i = 0; i < arr.size(); i++) {
                        JSONObject obj = (JSONObject) arr.get(i);
                        System.out.println("obj:"+obj);
                        if (obj.get("project").equals(project)) {
                            alreadyExist = true;
                            Integer sum = obj.getInt("duration");
                            sum += duration;
                            obj.put("duration", sum);
                            if(StringUtils.isNotBlank(reportContent)){
                                String reportContent1 = obj.getStr("reportContent");

                                if(StringUtils.isNotBlank(reportContent1)){
                                    reportContent1+=";"+reportContent;
                                }else{
                                    reportContent1=reportContent;
                                }
                                obj.put("reportContent", reportContent1);
                            }
                            break;
                        }
                    }
                    if (!alreadyExist) {
                        JSONObject obj = new JSONObject();
                        obj.put("project", project);
                        obj.put("duration", duration);
                        if(StringUtils.isNotBlank(reportContent)){
                            obj.put("reportContent", reportContent);
                        }
                        arr.add(obj);
                    }
                }
            }
            res.put(type, arr);
        }
        return res;
    }

}
