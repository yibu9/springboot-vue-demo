package com.mvp.controller;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mvp.common.Result;
import com.mvp.entity.Task;
import com.mvp.entity.TaskDTO;
import com.mvp.mapper.TaskMapper;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/task")
public class TaskController {

    @Resource
    private TaskMapper taskMapper;

    @PostMapping
    public Result<?> save(@RequestBody Task task){
        System.out.println("save task:"+task);
        task.setLevel(0);
        String importance = task.getImportance();

        if("one".equals(importance)){
            task.setImportance("1");
            task.setUrgency("1");
        } else if ("two".equals(importance)) {
            task.setImportance("1");
            task.setUrgency("0");
        } else if ("three".equals(importance)) {
            task.setImportance("0");
            task.setUrgency("0");
        } else if ("four".equals(importance)) {
            task.setImportance("0");
            task.setUrgency("1");
        } else{
            return Result.error("-1","importance is not one,two,three,four");
        }
        String progress = task.getProgress();
        if(StringUtils.isBlank(progress) || (!"0".equals(progress) && !"1".equals(progress) && !"2".equals(progress))){
            task.setProgress("0");
        }
        taskMapper.insert(task);
        return Result.success();
    }

    @PostMapping("/addSub")
    public Result<?> saveSub(@RequestBody Task task){
        System.out.println("save subTask:"+task);
        String fatherId = task.getFather();
        Task father = taskMapper.selectById(Integer.valueOf(fatherId));
        if(father == null){
            return Result.error("-1","father task is not exist");
        }
        int fatherLevel = father.getLevel();
        task.setLevel(fatherLevel + 1);
        taskMapper.insert(task);
        return Result.success();
    }
    @GetMapping("/done")
    public Result<?> done(@RequestParam String id){
        System.out.println("done:"+id);
        taskMapper.updateProgressById(2, Integer.valueOf(id));
        return Result.success();
    }
    @GetMapping("/start")
    public Result<?> start(@RequestParam String id){
        System.out.println("start:"+id);
        taskMapper.updateProgressById(1, Integer.valueOf(id));
        return Result.success();
    }

    @GetMapping("/stop")
    public Result<?> stop(@RequestParam String id){
        System.out.println("stop:"+id);
        taskMapper.updateProgressById(0, Integer.valueOf(id));
        return Result.success();
    }


    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Long id){
        taskMapper.deleteById(id);
        return Result.success();
    }

    @PutMapping
    public Result<?> update(@RequestBody Task task){
        taskMapper.updateById(task);
        return Result.success();
    }

    @GetMapping("/getTaskByUserAndStatus")
    public Result<?> getTaskByUserAndStatus(@RequestParam(defaultValue = "") String username) throws ParseException {
        if(StringUtils.isBlank(username)){
            return Result.error("-1","username is null");
        }
        List<Task> taskByUserAndProgress = taskMapper.getTaskByUserAndProgress(username, "1");
        return Result.success(taskByUserAndProgress);
    }


    @GetMapping
    public Result<?> findPage(
            @RequestParam(defaultValue = "") String username) throws ParseException {
        System.out.println("username:"+username);
        LambdaQueryWrapper<Task> wrapper = Wrappers.<Task>lambdaQuery();

        if(StringUtils.isNotBlank(username)){
            wrapper.eq(Task::getUser, username);
        }else{
            wrapper.eq(Task::getUser, "unknown");
            System.out.println("用户名未知");
        }
        wrapper.eq(Task::getLevel, 0);//先查出所有的层级为0的任务
//        wrapper.ne(Task::getProgress,2);
        wrapper.orderByAsc(Task::getDdl);
        List<Task> tasks = taskMapper.selectList(wrapper);
        Integer maxLevel = taskMapper.getMaxLevelByUser(username);
        ArrayList<TaskDTO> res = new ArrayList<>();
        if(maxLevel != null){
            //遍历tasks
            for(Task task : tasks){
                TaskDTO taskDTO = new TaskDTO(task);
                wrapper.clear();
                wrapper.eq(Task::getFather, taskDTO.getId());//查找当前任务的儿子
                wrapper.eq(Task::getUser, username);//用户名需要保持一致
                wrapper.orderByAsc(Task::getDdl);
                wrapper.ne(Task::getProgress,2);
                List<Task> sons = taskMapper.selectList(wrapper);
                ArrayList<TaskDTO> sonDTOS = changeTaskList2DTOList(sons);
                //还需要对taskDTO的children进行递归的查询子任务处理
                ArrayList<TaskDTO> deepSonDTOS = RecursiveSearch(sonDTOS, maxLevel,username);
                System.out.println("&&&&:"+deepSonDTOS);
                taskDTO.setChildren(deepSonDTOS);
                res.add(taskDTO);
            }
        }
        System.out.println("*****res:"+res);

        ArrayList<TaskDTO> one = new ArrayList<>();
        ArrayList<TaskDTO> two = new ArrayList<>();
        ArrayList<TaskDTO> three = new ArrayList<>();
        ArrayList<TaskDTO> four = new ArrayList<>();

        for(TaskDTO taskDTO : res){
            if("1".equals(taskDTO.getImportance())){ //一二象限
                    if("1".equals(taskDTO.getUrgency())){ //重要紧急
                        one.add(taskDTO);
                    }else if("0".equals(taskDTO.getUrgency())){//重要不紧急
                        two.add(taskDTO);
                    }
            } else if ("0".equals(taskDTO.getImportance())) { //三四象限
                if("1".equals(taskDTO.getUrgency())){ //不重要紧急
                        four.add(taskDTO);
                }else if("0".equals(taskDTO.getUrgency())){ //不重要不紧急
                        three.add(taskDTO);
                }
            }
        }
        JSONObject resObj = new JSONObject();
        resObj.put("one",one);
        resObj.put("two",two);
        resObj.put("three",three);
        resObj.put("four",four);
        return Result.success(resObj);
    }

    /**
     * 递归的查询dto的孩子任务
     * @param TaskDTOs
     * @param maxLevel
     * @return
     */
    private ArrayList<TaskDTO> RecursiveSearch(List<TaskDTO> TaskDTOs,Integer maxLevel,String username){
        System.out.println("@@@@递归遍历");
        System.out.println("@@@@TaskDTOs:"+TaskDTOs);
        System.out.println("@@@@maxLevel:"+maxLevel);
        System.out.println("@@@@username:"+username);
        ArrayList<TaskDTO> res = new ArrayList<>();
        LambdaQueryWrapper<Task> wrapper = Wrappers.<Task>lambdaQuery();
        //遍历TaskDTOs
        for(TaskDTO taskDTO : TaskDTOs){
            Integer level = taskDTO.getLevel();
            if(level<=maxLevel){
                wrapper.clear();
                wrapper.eq(Task::getFather, taskDTO.getId());//查找当前任务的儿子
                wrapper.eq(Task::getUser, username);//用户名需要保持一致
                List<Task> sons = taskMapper.selectList(wrapper);
                if(sons.size()!=0){
                    ArrayList<TaskDTO> sonDTOS = changeTaskList2DTOList(sons);
                    ArrayList<TaskDTO> deepSonDTOS = RecursiveSearch(sonDTOS,maxLevel,username);
                    System.out.println("###deepSonDTOS:"+deepSonDTOS);
                    taskDTO.setChildren(deepSonDTOS);
                }
            }
            res.add(taskDTO);
        }
        System.out.println("###res:"+res);
        return res;
    }

    private ArrayList<TaskDTO> changeTaskList2DTOList(List<Task> tasks){
        ArrayList<TaskDTO> res = new ArrayList<>();
        for(Task task : tasks){
            TaskDTO taskDTO = new TaskDTO(task);
            res.add(taskDTO);
        }
        return res;
    }
}
