package com.mvp.controller;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mvp.common.PubFun;
import com.mvp.common.Result;
import com.mvp.entity.Habit;
import com.mvp.entity.HabitLog;
import com.mvp.mapper.HabitLogMapper;
import com.mvp.mapper.HabitMapper;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.mvp.common.Result.success;

@RestController
@RequestMapping("/habit")
public class HabitController {

    @Resource
    private HabitMapper habitMapper;

    @Resource
    private HabitLogMapper habitLogMapper;


    /**
     * 新增一个习惯
     * @param habit
     * @return
     */
    @PostMapping
    public Result<?> save(@RequestBody Habit habit){
        System.out.println("save habit:"+habit);
        if(StringUtils.isBlank(habit.getUser())){
            return Result.error("1001","user不能为空");
        }
        if(StringUtils.isBlank(habit.getName())){
            return Result.error("1001","habitName不能为空");
        }
        habit.setCreateTime(new Date());
        habitMapper.insert(habit);
        return success();
    }

    /**
     * 删除一个习惯
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Long id){
        Habit habit = habitMapper.selectById(id);
        if(habit == null){
            return Result.error("1001","id不存在");
        }
        habit.setIsDeleted("1");
        habitMapper.updateById(habit);
        return success();
    }

    /**
     * 更新一个习惯
     * @param habit
     * @return
     */
    @PutMapping
    public Result<?> update(@RequestBody Habit habit){
        habitMapper.updateById(habit);
        return success();
    }



    @PutMapping("/habitLog")
    public Result<?> updateHabitLog(@RequestBody HabitLog habitLog){
        habitLogMapper.updateById(habitLog);
        return success();
    }

    @PostMapping("/habitLog")
    public Result<?> saveHabitLog(@RequestBody HabitLog habitLog){
        System.out.println("save habitLog:"+habitLog);
        if(StringUtils.isBlank(habitLog.getUser())){
            return Result.error("1001","user不能为空");
        }
        if(StringUtils.isBlank(habitLog.getHabitName())){
            return Result.error("1001","habitName不能为空");
        }
        habitLogMapper.insert(habitLog);
        return success();
    }

    @DeleteMapping("/habitLog/{id}")
    public Result<?> deleteHabitLog(@PathVariable Long id){
        habitLogMapper.deleteById(id);
        return success();
    }


    @GetMapping("/finish")
    public Result<?> fingOneLog(@RequestParam(defaultValue = "") String user,
                                @RequestParam(defaultValue = "") String habitId){
        //校验
        if(StringUtils.isBlank(user)){
            return Result.error("-1","user不能为空");
        }
        if(StringUtils.isBlank(habitId)){
            return Result.error("-1","habitId不能为空");
        }

        Integer habitIdInt = Integer.parseInt(habitId);

        //拿到该习惯，检查是否是属于该用户
        Habit habit = habitMapper.selectById(habitId);
        if(!user.equals(habit.getUser())){
            return Result.error("-1","该习惯不属于该用户");
        }

        //检查是否存在今天的该用户的该习惯的记录，时间的参数要控制好
        Date beginOfTheDay = PubFun.getBeginOfTheDay(new Date());
        Date endOfTheDay = PubFun.getEndOfTheDay(new Date());
        LambdaQueryWrapper<HabitLog> habitLogWrapper = Wrappers.<HabitLog>lambdaQuery();
        habitLogWrapper.eq(HabitLog::getUser,user);//用户要一致
        habitLogWrapper.gt(HabitLog::getDateTime,beginOfTheDay);
        habitLogWrapper.lt(HabitLog::getDateTime,endOfTheDay);
        habitLogWrapper.eq(HabitLog::getHabitId,habitIdInt);//习惯的id要一致
        HabitLog habitLog = habitLogMapper.selectOne(habitLogWrapper);

        //没有的话就新增，有的话就修改
        if(habitLog == null){
            System.out.println("习惯不存在，新增一个");
            //新增
            habitLog = new HabitLog();
            habitLog.setHabitId(habitIdInt);
            habitLog.setHabitName(habit.getName());
            habitLog.setUser(user);
            habitLog.setDateTime(new Date());
            habitLog.setType("1");
            habitLog.setStat("1");
            habitLogMapper.insert(habitLog);
        }else{
            System.out.println("习惯已经存在");
            return Result.error("-2","状态修改已完毕，无需重复点击~");
        }
        return Result.success();
    }

    @GetMapping("/getHabitLog")
    public Result<?> fingOneLog(@RequestParam(defaultValue = "") String date,
                                @RequestParam(defaultValue = "") String user,
                                @RequestParam(defaultValue = "") String habitName) throws ParseException {
        //打印参数
        System.out.println("fingOneLog-----");
        System.out.println("user:"+user);
        System.out.println("habitName:"+habitName);
        System.out.println("date:"+date);

        if(StringUtils.isBlank(date)){
            return Result.error("-1","date不能为空");
        }
        if(StringUtils.isBlank(user)){
            return Result.error("-1","user不能为空");
        }
        if(StringUtils.isBlank(habitName)){
            return Result.error("-1","habitName不能为空");
        }

        LambdaQueryWrapper<HabitLog> wrapper = Wrappers.<HabitLog>lambdaQuery();
        wrapper.eq(HabitLog::getHabitName,habitName);
        wrapper.eq(HabitLog::getUser,user);
        wrapper.eq(HabitLog::getDateTime,date);

        HabitLog habitLog = habitLogMapper.selectOne(wrapper);
        return success(habitLog);

    }


    @GetMapping
    public Result<?> findHabitsAndLogs(
            @RequestParam(defaultValue = "") String date,
            @RequestParam(defaultValue = "") String user,
            @RequestParam(defaultValue = "") String numOfDays
    ) throws ParseException {
        System.out.println("findHabitsAndLogs-----");
        System.out.println("date:"+date);
        System.out.println("user:"+user);
        System.out.println("numOfDays:"+numOfDays);

        //校验参数合法性
        if(StringUtils.isBlank(user)){
            return Result.error("-1","user不能为空");
        }
        if(StringUtils.isBlank(numOfDays)){
            return Result.error("-1","numOfDays不能为空");
        }
        int sumDays = Integer.parseInt(numOfDays);

        //如果日期为空，则默认为今天
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("MMdd");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        Date day = new Date();
        if(StringUtils.isNotBlank(date)){
            day = sdf.parse(date);
        }

        //根据天数得到起始日期和结束日期
        System.out.println("day:"+day.getTime());
        System.out.println("sub:"+24*60*60*1000*Integer.parseInt(numOfDays));
        System.out.println("day:"+day);

        //得到数组列表
        List<String> dateArr = new ArrayList();
        long time = day.getTime();

        time = time - 24*60*60*1000*Integer.parseInt(numOfDays)+ 24*60*60*1000;
        for (int i = 0; i < sumDays; i++) {
            Date date1 = new Date(time);
            String format = sdf2.format(date1);
            dateArr.add(format);
            time = time + 24*60*60*1000;
        }

        Date startDate = new Date(day.getTime() - 24*60*60*1000*Integer.parseInt(numOfDays));
        Date endDate = new Date(day.getTime() + 24*60*59*1000);
        System.out.println("@@@起始日期："+startDate);
        System.out.println("@@@结束日期："+endDate);

        //得到该用户的所有习惯
        LambdaQueryWrapper<Habit> habitWrapper = Wrappers.<Habit>lambdaQuery();
        habitWrapper.eq(Habit::getUser,user);//用户作为条件
        habitWrapper.eq(Habit::getIsDeleted,"0");//未删除的习惯才能被查询出来
        List<Habit> habits = habitMapper.selectList(habitWrapper);

        JSONArray arr = new JSONArray();
        for (Habit habit : habits){
            Date createTime = habit.getCreateTime();
            //为每一个习惯查询出所有的日志，然后构造json对象，这样数据库交互次数太多，后期要做实验，看一次性读取出来在内存中处理和多次查询数据库能差多少
            LambdaQueryWrapper<HabitLog> habitLogWrapper = Wrappers.<HabitLog>lambdaQuery();
            habitLogWrapper.eq(HabitLog::getHabitName,habit.getName());
            habitLogWrapper.eq(HabitLog::getUser,user);
            habitLogWrapper.gt(HabitLog::getDateTime,startDate);
            habitLogWrapper.lt(HabitLog::getDateTime,endDate);
            List<HabitLog> habitLogs = habitLogMapper.selectList(habitLogWrapper);
            int finishCount = 0;

            JSONObject habitObj = new JSONObject();
            habitObj.put("name",habit.getName());
            habitObj.put("habitId",habit.getId());
            for (HabitLog log : habitLogs){
                String logDateTime = sdf2.format(log.getDateTime());
                String logStat = log.getStat();
                if("1".equals(logStat)){
                    habitObj.put(logDateTime,"√");
                }else if("0".equals(logStat)){
                    habitObj.put(logDateTime,"X");
                }else{
                    habitObj.put(logDateTime,"X");
                }
                if(logStat.equals("1")){
                    finishCount++;
                }
            }

            String habitCreateTime = sdf2.format(createTime);
            for(String dateStr :dateArr){
                if(!habitObj.containsKey(dateStr)){
                    if(dateStr.compareTo(habitCreateTime)>0){
                        //如果日期大于习惯的创建日期,赋值为X
                        habitObj.put(dateStr,"X");
                    }else {
                        //如果日期小于等于习惯的创建日期,赋值为-
                        habitObj.put(dateStr,"-");
                    }

                }
            }

            //计算每个习惯的完成率
            double rate = (double)finishCount/sumDays;
            habitObj.put("rate",rate);
            arr.add(habitObj);
        }

        JSONObject result = new JSONObject();
        result.put("arr",arr);

        result.put("dateArr",dateArr);
        return success(result);
    }

}
