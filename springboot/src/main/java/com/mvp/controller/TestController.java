package com.mvp.controller;

import com.mvp.mapper.TAreaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//这是测试用的类，跟我自己的项目没啥关系
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TAreaMapper tAreaMapper;

    @GetMapping("/test1")
    public Map<Integer, Map<String, Map<String, String>>> getRelatedAreaNames() {
        // 执行自定义SQL查询
        List<Map<String, Object>> allAreas = tAreaMapper.getRelatedAreaNamesBySql();

        // 用于存储最终结果的Map，最外层以孙子的id为键
        Map<Integer, Map<String, Map<String, String>>> resultMap = new HashMap<>();

        // 遍历查询结果，将相关的area_name存到map中
        for (Map<String, Object> areaMap : allAreas) {
            Integer grandsonId = (Integer) areaMap.get("grandson_id");
            String grandsonAreaName = (String) areaMap.get("grandson_area_name");
            String fatherAreaName = (String) areaMap.get("father_area_name");
            String grandfatherAreaName = (String) areaMap.get("grandfather_area_name");

            if (grandsonId!= null) {
                // 如果最外层Map中不存在以当前孙子id为键的记录，则创建一个新的内层Map
                Map<String, Map<String, String>> innerMap = resultMap.computeIfAbsent(grandsonId, k -> new HashMap<>());

                // 将相关的area_name存到内层Map中
                Map<String, String> areaNameMap = innerMap.computeIfAbsent(grandsonAreaName, k -> new HashMap<>());
                areaNameMap.put(fatherAreaName, grandfatherAreaName);
            }
        }

        return resultMap;
    }



    @GetMapping("/test2")
    public Map<Integer, String> getRelatedAreaNamesCombined() {
        // 执行自定义SQL查询
        List<Map<String, Object>> allAreas = tAreaMapper.getRelatedAreaNamesBySql();

        // 用于存储最终结果的Map，键是孙子的id，值是拼接后的area_name字符串
        Map<Integer, String> resultMap = new HashMap<>();

        // 遍历查询结果，拼接area_name并存储到map中
        for (Map<String, Object> areaMap : allAreas) {
            Integer grandsonId = (Integer) areaMap.get("grandson_id");
            String grandsonAreaName = (String) areaMap.get("grandson_area_name");
            String fatherAreaName = (String) areaMap.get("father_area_name");
            String grandfatherAreaName = (String) areaMap.get("grandfather_area_name");

            if (grandsonId!= null) {
                // 拼接孙子、父亲、爷爷的area_name
                String combinedAreaNames = grandsonAreaName +"-"+ fatherAreaName +"-"+ grandfatherAreaName;

                // 将拼接后的字符串存储到结果Map中，以孙子的id为键
                resultMap.put(grandsonId, combinedAreaNames);
            }
        }

        return resultMap;
    }
}
