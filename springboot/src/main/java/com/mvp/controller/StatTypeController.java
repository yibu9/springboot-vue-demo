package com.mvp.controller;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.mvp.common.Result;
import com.mvp.entity.Log;
import com.mvp.mapper.LogMapper;
import com.mvp.util.ColorUtil;
import com.mvp.util.TimeUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/statType")
public class StatTypeController {
    @Resource
    private LogMapper logMapper;

    private Map<String,String> colorMap = new HashMap<>();

    @GetMapping("/categoryData")
    public Result<?> categoryData(
            @RequestParam(required = true) String username,
            @RequestParam(required = true) String view
    ) {
        System.out.println("---------categoryData.username-------");
        System.out.println(username);
        System.out.println("---------categoryData.view-------");
        System.out.println(view);
        boolean isSum = false;
        if("sum".equals(view)){
            isSum = true;
        }

        //校验用户名
        if(StringUtils.isBlank(username)){
            return Result.error("-1","用户名不能为空");
        }
        JSONObject res = new JSONObject();
        LocalDate now = LocalDate.now();
        // 获取本年度的数据

        LocalDate[] lastYearRange = TimeUtil.getLastYearRange();
        List<Log> lastYearData = getDataInRange(username,lastYearRange[0], lastYearRange[1]);
        long betweenLastYear = ChronoUnit.DAYS.between(lastYearRange[0], lastYearRange[1]);

        LocalDate[] thisYearRange = {TimeUtil.getStartOfYear(now), TimeUtil.getEndOfYear(now)};
        List<Log> thisYearData = getDataInRange(username,thisYearRange[0], thisYearRange[1]);
        long betweenThisYear = ChronoUnit.DAYS.between(thisYearRange[0], now);

        int count = 0;
        int coutLastYear = lastYearData.size();
        int countThisYear = thisYearData.size();
        List<Log> mostLargeYear = null;

        if(countThisYear > coutLastYear){
            //今年数据更多
            mostLargeYear = thisYearData;
        }else{
            //去年的数据更完善
            mostLargeYear = lastYearData;
        }

        for(Log log : mostLargeYear){
            String key = log.getLargeCategory();
            if(StringUtils.isBlank(key)){
                key = "未分类";
            }
            if(!colorMap.containsKey(key)){
                colorMap.put(key, ColorUtil.getColorByIndex(count++));
            }
        }


        fillData(res,lastYearData,"lastYear",betweenLastYear,isSum);
        fillData(res,thisYearData,"thisYear",betweenThisYear,isSum);

        //本周
        LocalDate[] thisWeekRange = {TimeUtil.getStartOfWeek(now), TimeUtil.getEndOfWeek(now)};
        List<Log> thisWeekData = getDataInRange(username,thisWeekRange[0], thisWeekRange[1]);
        long betweenThisWeek = ChronoUnit.DAYS.between(thisWeekRange[0], now);
        fillData(res,thisWeekData,"thisWeek",betweenThisWeek,isSum);

        // 获取上周的数据
        LocalDate[] lastWeekRange = TimeUtil.getLastWeekRange();
        List<Log> lastWeekData = getDataInRange(username,lastWeekRange[0], lastWeekRange[1]);
        long betweenLastWeek = ChronoUnit.DAYS.between(lastWeekRange[0], lastWeekRange[1]);
        fillData(res,lastWeekData,"lastWeek",betweenLastWeek,isSum);

        // 获取上个月的数据
        LocalDate[] lastMonthRange = TimeUtil.getLastMonthRange();
        List<Log> lastMonthData = getDataInRange(username,lastMonthRange[0], lastMonthRange[1]);
        long betweenLastMonth = ChronoUnit.DAYS.between(lastMonthRange[0], lastMonthRange[1]);
        fillData(res,lastMonthData,"lastMonth",betweenLastMonth,isSum);

        // 获取本月的数据
        LocalDate[] thisMonthRange = {TimeUtil.getStartOfMonth(now), TimeUtil.getEndOfMonth(now)};
        List<Log> thisMonthData = getDataInRange(username,thisMonthRange[0], thisMonthRange[1]);
        long betweenThisMonth = ChronoUnit.DAYS.between(thisMonthRange[0], now);
        fillData(res,thisMonthData,"thisMonth",betweenThisMonth,isSum);

        // 获取本季度的数据
//        LocalDate[] thisQuarterRange = {TimeUtil.getStartOfQuarter(now), TimeUtil.getEndOfQuarter(now)};
//        List<Log> thisQuarterData = getDataInRange(username,thisQuarterRange[0], thisQuarterRange[1]);
//        long betweenThisQuarter = ChronoUnit.DAYS.between(thisQuarterRange[0], now);
//        fillData(res,thisQuarterData,"thisQuarter",betweenThisQuarter);

        // 获取上个季度的数据
//        LocalDate[] lastQuarterRange = TimeUtil.getLastQuarterRange();
//        List<Log> lastQuarterData = getDataInRange(username,lastQuarterRange[0], lastQuarterRange[1]);
//        long betweenLastQuarter = ChronoUnit.DAYS.between(lastQuarterRange[0], lastQuarterRange[1]);
//        fillData(res,lastQuarterData,"lastQuarter",betweenLastQuarter);

        //计算涨幅
        calculateRate(res);

        System.out.println("------------categoryData.res-------");
        System.out.println(res);
        return Result.success(res);
    }

    private void calculateRate(JSONObject res) {
        System.out.println("---------calculateRate--------------");
        int lastYearSumMintus = res.getInt("lastYearSumMintus");
        int thisYearSumMintus = res.getInt("thisYearSumMintus");
        int lastWeekSumMintus = res.getInt("lastWeekSumMintus");
        int thisWeekSumMintus = res.getInt("thisWeekSumMintus");
        int lastMonthSumMintus = res.getInt("lastMonthSumMintus");
        int thisMonthSumMintus = res.getInt("thisMonthSumMintus");

        if(lastYearSumMintus == 0){
            res.put("yearRate","");
        }else{
            double yearRate = (double) (thisYearSumMintus - lastYearSumMintus) / lastYearSumMintus;
            System.out.println("thisYearSumMintus"+thisYearSumMintus);
            System.out.println("lastYearSumMintus"+lastYearSumMintus);
            System.out.println("yearRate"+yearRate);
            res.put("yearRate"," 增幅 "+toPercentString(yearRate));
        }

        if(lastWeekSumMintus == 0){
            res.put("weekRate","");
        }else{
            double weekRate = (double) (thisWeekSumMintus - lastWeekSumMintus) / lastWeekSumMintus;
            res.put("weekRate"," 增幅 "+ toPercentString(weekRate));
        }

        if(lastMonthSumMintus == 0){
            res.put("monthRate","");
        }else{
            double monthRate = (double) (thisMonthSumMintus - lastMonthSumMintus) / lastMonthSumMintus;
            res.put("monthRate"," 增幅 "+toPercentString(monthRate));
        }
    }


    public static String toPercentString(double value) {
        // 使用 String.format 方法
        return String.format("%.2f%%", value * 100);

        // 或者使用 DecimalFormat 方法
        // DecimalFormat df = new DecimalFormat("0.00");
        // return df.format(value * 100) + "%";
    }

    @GetMapping("/categoryDataBackUp")
    public Result<?> categoryDataBackUp(
            @RequestParam(required = true) String username
    ) {
        //校验用户名
        if(StringUtils.isBlank(username)){
            return Result.error("-1","用户名不能为空");
        }

        List<Log> last7daysCategory = logMapper.getLastNdaysCategory(username, 7);
        JSONArray last7daysCategoryData = new JSONArray();
        HashMap<String,Integer> map7Days = new HashMap<>();
        int thisWeekSum = 0;
        for (Log log : last7daysCategory){
            String key = log.getLargeCategory();
            if(StringUtils.isBlank(key)){
                key = "未分类";
            }
            if(map7Days.containsKey(key)){
                map7Days.put(key,map7Days.get(key)+log.getDuration());
            }else{
                map7Days.put(key,log.getDuration());
            }
            thisWeekSum += log.getDuration();
        }
        Set<String> strings7 = map7Days.keySet();
        String[] names7 = new String[strings7.size()];
        int index = 0;
        for (String key : strings7) {
            names7[index++] = key;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",key);
            jsonObject.put("value",map7Days.get(key));
            last7daysCategoryData.add(jsonObject);
        }

        List<Log> last30daysCategory = logMapper.getLastNdaysCategory(username, 30);
        JSONArray last30daysCategoryData = new JSONArray();
        HashMap<String,Integer> map30Days = new HashMap<>();
        for (Log log : last30daysCategory){
            String key = log.getLargeCategory();
            if(StringUtils.isBlank(key)){
                key = "未分类";
            }
            if(map30Days.containsKey(key)){
                map30Days.put(key,map30Days.get(key)+log.getDuration());
            }else{
                map30Days.put(key,log.getDuration());
            }
        }
        Set<String> strings30 = map30Days.keySet();
        String[] names30 = new String[strings30.size()];
        index = 0;
        for (String key : strings30) {
            names30[index++] = key;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",key);
            jsonObject.put("value",map30Days.get(key));
            last30daysCategoryData.add(jsonObject);
        }

        List<Log> last90daysCategory = logMapper.getLastNdaysCategory(username, 90);
        JSONArray last90daysCategoryData = new JSONArray();
        HashMap<String,Integer> map90Days = new HashMap<>();
        for (Log log : last90daysCategory){
            String key = log.getLargeCategory();
            if(StringUtils.isBlank(key)){
                key = "未分类";
            }
            if(map90Days.containsKey(key)){
                map90Days.put(key,map90Days.get(key)+log.getDuration());
            }else{
                map90Days.put(key,log.getDuration());
            }
        }
        Set<String> strings90 = map90Days.keySet();
        String[] names90 = new String[strings90.size()];
        index = 0;
        for (String key : strings90) {
            names90[index++] = key;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",key);
            jsonObject.put("value",map90Days.get(key));
            last90daysCategoryData.add(jsonObject);
        }

        List<Log> last365daysCategory = logMapper.getLastNdaysCategory(username, 365);
        JSONArray last365daysCategoryData = new JSONArray();
        HashMap<String,Integer> map365Days = new HashMap<>();
        for (Log log : last365daysCategory){
            String key = log.getLargeCategory();
            if(StringUtils.isBlank(key)){
                key = "未分类";
            }
            if(map365Days.containsKey(key)){
                map365Days.put(key,map365Days.get(key)+log.getDuration());
            }else{
                map365Days.put(key,log.getDuration());
            }
        }
        Set<String> strings365 = map365Days.keySet();
        String[] names365 = new String[strings365.size()];
        index = 0;
        for (String key : strings365) {
            names365[index++] = key;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",key);
            jsonObject.put("value",map365Days.get(key));
            last365daysCategoryData.add(jsonObject);
        }

        JSONObject res = new JSONObject();
        res.set("thisWeekSum", TimeUtil.formatMinutesToHourMin(thisWeekSum));

        res.put("last7daysCategoryData",last7daysCategoryData);
        res.put("last30daysCategoryData",last30daysCategoryData);
        res.put("last90daysCategoryData",last90daysCategoryData);
        res.put("last365daysCategoryData",last365daysCategoryData);

        res.put("last7daysCategoryNames",names7);
        res.put("last30daysCategoryNames",names30);
        res.put("last90daysCategoryNames",names90);
        res.put("last365daysCategoryNames",names365);

        return Result.success(res);
    }


    public List<Log> getDataInRange(String username , LocalDate startDate, LocalDate endDate)   {
        List<Log> Logs = logMapper.selectByRange(username, startDate, endDate);
        return Logs;
    }

    public void fillData(JSONObject result, List<Log> Logs,String dataName,long betweenDays,boolean isSum){
        betweenDays = betweenDays+1;
        JSONArray arr = new JSONArray();
        HashMap<String,Integer> map = new HashMap<>();
        int sumMintus = 0;
        for (Log log : Logs){
            String key = log.getLargeCategory();
            if(StringUtils.isBlank(key)){
                key = "未分类";
            }
            if(map.containsKey(key)){
                map.put(key,map.get(key)+log.getDuration());
            }else{
                map.put(key,log.getDuration());
            }
            sumMintus += log.getDuration();
        }
        Set<String> strings = map.keySet();
        String[] names = new String[strings.size()];
        int index = 0;
        for (String key : strings) {
            names[index++] = key;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name",key);
            if(!isSum){
                jsonObject.put("value",map.get(key)/betweenDays);
            }else{
                jsonObject.put("value",map.get(key));
            }
            jsonObject.put("itemStyle",colorObj(key));
            arr.add(jsonObject);
        }
        result.set(dataName+"Data",arr);
        result.set(dataName+"Sum",TimeUtil.formatMinutesToHourMin(sumMintus));
        String AvgNum = TimeUtil.formatMinutesToHourMin((int) (sumMintus / betweenDays));
        result.set(dataName+"SumMintus",sumMintus/betweenDays);
        result.set(dataName+"Avg",AvgNum+"（"+betweenDays+"天）");
    }

    public JSONObject colorObj(String name){
        String color = colorMap.get(name);
        JSONObject obj = new JSONObject();
        obj.put("color",color);
        return obj;
    }
}
