package com.mvp.common;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class PubFun {

    /**
     * 传入格式化和过去的天数，返回第N天前的日期
     * @param formatter
     * @param minusDay
     * @return
     */
    public static String getDayStrByFormatterAndMinusDay(String formatter, int minusDay){
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter df = DateTimeFormatter.ofPattern(formatter);
        localDateTime = localDateTime.minusDays(minusDay);
        return df.format(localDateTime);
    };


    /**
     * 传入格式化的模版和天数，将过去的天数按照模版格式化，返回过去N天的日期
     * @param days
     * @param formatter
     * @return
     */
    public static List<String> getLastDaysByFormatter(int days, String formatter){
        System.out.println("days:"+days);
        System.out.println("formatter:"+formatter);
        ArrayList<String> list = new ArrayList<>();

        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter df = DateTimeFormatter.ofPattern(formatter);

        for (int i = 0; i < days; i++) {
            list.add(df.format(localDateTime));
            localDateTime = localDateTime.minusDays(1);
        }
        Collections.reverse(list);
        return list;
    }


    public static Date getBeginOfTheDay(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static  Date getEndOfTheDay(Date date){
        long l = getBeginOfTheDay(date).getTime() + 24 * 60 * 60 * 1000 - 1;
        return new Date(l);
    }

}
