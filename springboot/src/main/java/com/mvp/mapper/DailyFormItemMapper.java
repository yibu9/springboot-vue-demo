package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.DailyFormItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Date;

@Mapper
public interface DailyFormItemMapper extends BaseMapper<DailyFormItem> {

    @Select("SELECT count(*) FROM `daily_form_item` where username=#{username} and Date(date_time)=Date(#{date})")
    public Integer getTodayFormItemCount(String username, Date date);


    @Select("update daily_form_item set value = #{value},content = #{content} where id = #{id}")
    public Integer updateStringById(String value,String content,Integer id);
    @Select("update daily_form_item set value = #{value},data_value = #{dataValue} where id = #{id}")
    public Integer updateDataValueById(String value,String dataValue,Integer id);
    @Select("update daily_form_item set value = #{value},data_time = #{DateTime} where id = #{id}")
    public Integer updateDateTimeById(String value,String DateTime,Integer id);
    @Select("update daily_form_item set value = #{value},boolean_value = #{value} where id = #{id}")
    public Integer updateBooleanById(String value,String booleanValue,Integer id);
    @Select("update daily_form_item set value = #{value},selected = #{selected} where id = #{id}")
    public Integer updateSelectById(String value,String selected,Integer id);

    @Update("update daily_form_item set deleted=1 where prototype_id = #{prototypeId}")
    public Integer deleteByPrototypeId(int prototypeId);

}