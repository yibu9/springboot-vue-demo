package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.HabitLog;


public interface HabitLogMapper  extends BaseMapper<HabitLog> {
}
