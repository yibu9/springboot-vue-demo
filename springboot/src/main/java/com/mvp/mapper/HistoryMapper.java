package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.Log;
import org.apache.ibatis.annotations.Update;

public interface HistoryMapper extends BaseMapper<Log> {



    @Update("update `t_log` set type = #{value} where user = #{username} and type=#{type}")
    int updateTypeByUser(String username, String type,String value);

    @Update("update `t_log` set project = #{value} where user = #{username} and project=#{project}")
    int updateProjectByUser(String username, String project,String value);

}
