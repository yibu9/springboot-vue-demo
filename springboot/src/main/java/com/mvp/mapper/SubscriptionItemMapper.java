package com.mvp.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.SubscriptionItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SubscriptionItemMapper extends BaseMapper<SubscriptionItem> {

    @Select("SELECT DISTINCT large_category FROM t_log WHERE user= #{user}")
    List<String> getNamesByLargeCategoryAndUser(@Param("user") String user);

    @Select("SELECT DISTINCT type FROM t_log WHERE user= #{user}")
    List<String> getNamesByTypeAndUser( @Param("user") String user);

    @Select("SELECT DISTINCT project FROM t_log WHERE user= #{user}")
    List<String> getNamesByProjectAndUser( @Param("user") String user);
//`subscription_type`,`name`,`username`,`subscription_time`

    @Update("UPDATE subscription_item " +
            "SET remarks = #{remarks}, sort_weight = #{sortWeight}, target_time = #{targetTime} " +
            "WHERE subscription_type = #{subscriptionType} " +
            "AND name = #{name} " +
            "AND username = #{username} " +
            "AND subscription_time = #{subscriptionTime}")
    int updateByKeys(SubscriptionItem item);
}