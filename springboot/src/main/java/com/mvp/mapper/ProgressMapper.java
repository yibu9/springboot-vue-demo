package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.Progress;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ProgressMapper extends BaseMapper<Progress> {

    @Select("select `order_num` from t_progress where user = #{user} order by `order_num` desc limit 1")
    Integer getMaxOrderByUser(String user);

    @Select("SELECT * FROM `t_progress` where order_num < #{orderNum} and user = #{user} order by order_num desc limit 1")
    Progress selectMaxOrderNumProgressUnderThisByUser(String user,Integer orderNum);

    @Select("SELECT * FROM `t_progress` where order_num > #{orderNum} and user = #{user} order by order_num asc limit 1")
    Progress selectMinOrderNumProgressAboveThisByUser(String user,Integer orderNum);

    /**
     * 根据 progress 对象的 id 更新记录。如果 progress 对象的时间字段为 null，则更新为数据库中的 NULL。
     *
     * @param progress 包含更新信息的 Progress 对象
     * @return 影响的行数
     */
    @Update({
            "<script>",
            "UPDATE t_progress",
            "SET",
            "target_value = #{progress.targetValue},",
            "current_value = #{progress.currentValue},",
            "percentage = #{progress.percentage},",
            "status = #{progress.status},",
            "content = #{progress.content},",
            "unit = #{progress.unit},",
            "expected_completion_time = ",
            "<if test='progress.expectedCompletionTime != null'>#{progress.expectedCompletionTime}</if>",
            "<if test='progress.expectedCompletionTime == null'>NULL</if>,",
            "evaluation = #{progress.evaluation},",
            "name = #{progress.name}",
            "WHERE id = #{progress.id}",
            "</script>"
    })
    int updateByIdSelective(@Param("progress") Progress progress);

//    @Select("SELECT type FROM `t_progress` where `user`=#{user} ")
    @Select("SELECT distinct type FROM `t_progress` where `user`=#{user} and (deleted !=1 or deleted is null)")
    List<String> getTypeListByUser(String user);

    @Update("update t_progress set deleted=1 where `user`=#{user} and id = #{id}")
    int deleteByIdAndUser(@Param("id") Integer id, @Param("user") String user);
}