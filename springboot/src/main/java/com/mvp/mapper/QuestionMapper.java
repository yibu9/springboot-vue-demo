package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.QuestionInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface QuestionMapper extends BaseMapper<QuestionInfo> {

    @Select("SELECT DISTINCT subject FROM question_info WHERE user_name = #{userName}")
    List<String> getSubjectsByUser(String userName);

    @Select("SELECT DISTINCT first_level_category FROM question_info WHERE user_name = #{userName}")
    List<String> getFirstLevelCategories(String userName);
}