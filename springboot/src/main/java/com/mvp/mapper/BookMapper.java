package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.Book;

public interface BookMapper extends BaseMapper<Book> {
}
