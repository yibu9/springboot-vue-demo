package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.ProgressItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProgressItemMapper extends BaseMapper<ProgressItem> {

    @Select("select * from t_progress_item where progress_id =#{progressId}")
    List<ProgressItem> selectProgressItemByProgressId(Integer progressId);

    @Select("SELECT COUNT(*) FROM t_progress_item WHERE progress_id = #{progressId}")
    long countByProgressId(@Param("progressId") Integer progressId);

    @Select("SELECT * FROM t_progress_item WHERE progress_id = #{progressId} order by date_time desc LIMIT #{offset}, #{size}")
    List<ProgressItem> selectByProgressId(@Param("progressId") Integer progressId,
                                          @Param("offset") int offset,
                                          @Param("size") int size);
}