package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.News;

public interface NewsMapper extends BaseMapper<News> {
}
