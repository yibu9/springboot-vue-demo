package com.mvp.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.Habit;

public interface HabitMapper  extends BaseMapper<Habit> {
}
