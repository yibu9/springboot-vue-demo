package com.mvp.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.Task;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface TaskMapper  extends BaseMapper<Task> {


    @Select("select level from task  where user = #{user}   ORDER BY level desc limit 1")
    Integer getMaxLevelByUser(String user);


    @Update("update task set progress = #{progress} where id = #{id}")
    void updateProgressById(Integer progress, Integer id);

    @Select("SELECT * FROM `task` where  user = #{user} and progress = #{progress} ORDER BY importance desc,urgency desc, level ")
    List<Task> getTaskByUserAndProgress(String user,String progress);

}
