package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.Log;
import com.mvp.entity.ValueBean;
import com.mvp.entity.WeekBean;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.List;

public interface LogMapper extends BaseMapper<Log> {

    @Select("select large_category,type,project,sum(duration) as duration from t_log where  date_time= #{date} and user=#{username}  group by project ")
    List<Log> selectStaticByUserAndDate(String date, String username);


    @Select("SELECT DISTINCT large_category  FROM `t_log`  where user = #{username} and large_category is not null order by id desc")
    List<String> getAllLargeCategory(String username);

    @Select("SELECT DISTINCT type  FROM `t_log`  where user = #{username} order by id desc")
    List<String> getTypeByUser(String username);

    @Select("SELECT DISTINCT project  FROM `t_log`  where user = #{username} order by id desc")
    List<String> getProjectByUser(String username);

    @Select("SELECT DISTINCT type  FROM `t_log`  where user = #{username} and large_category = #{largeCategory} order by id desc")
    List<String> getTypeByUserAndLargeCategory(String username, String largeCategory);

    @Select("SELECT DISTINCT project  FROM `t_log`  where user = #{username} and type = #{type} order by id desc")
    List<String> getProjectByUserAndType(String username,String type);

    @Select("SELECT distinct user FROM `t_log`")
    List<String> getAllAliveUser();

    @Select("SELECT user,sum(duration) as duration ,date(date_time) as date_time FROM `t_log`  where user = #{username} and date_time >= #{date} GROUP BY date(date_time) order by date_time")
    List<Log> selectLogsByUserAndDate(String date, String username);

    @Select("select distinct type from t_log where user = #{username}  and date_time >= #{date}")
    List<String> getAllTypeByUserAndDate(String date, String username);

    @Select("select type,sum(duration) as duration ,date(date_time) as date_time FROM `t_log`  where user = #{username} and date_time >= #{date} GROUP BY type,date(date_time) order by date_time")
    List<Log> selectLogsByUserAndDateAndType(String date, String username);

    @Select("select * from t_log where `user` =  #{username}  and date_time >=  #{beginDate}  and date_time <= #{endDate} order by date_time")
    List<Log> selectLogsByUserAndDateRange( String username,String beginDate,String endDate);


    //时机正确的时候，价值x紧急程度x难度x专注度x持续时间/60
    //时机正确的时候，价值x紧急程度x难度x专注度x持续时间*0.5/60

    @Select("SELECT\n" +
            "    DATE(date_time) AS date,\n" +
            "    SUM(CASE\n" +
            "            WHEN timing = 1 THEN  value * urgency * difficulty * focus * duration / 60\n" +
            "            ELSE   value * urgency * difficulty *  focus * 0.5 *duration / 60\n" +
            "        END) AS value,\n" +
            "\t\tuser\t\t\n" +
            "FROM\n" +
            "    t_log\n" +
            "WHERE\n" +
            "    user = #{username}\n" +
            "    AND date_time >= #{date}\n" +
            "GROUP BY\n" +
            "    DATE(date_time);")
    List<ValueBean> selectEveryDayValueByUser(String date, String username);

    @Select("SELECT sum(duration) FROM `t_log` where user=#{username} and date_time >= #{date}")
    Double selectSumTimeByUserAndBeginDate(String date, String username);

    @Select("SELECT large_category,sum(duration) as duration\n" +
            "FROM t_log \n" +
            "WHERE user = #{username}\n" +
            "  AND date_time >= NOW() - INTERVAL #{n} DAY\n" +
            "GROUP BY large_category;")
    List<Log> getLastNdaysCategory(String username,Integer n);

    @Select("SELECT large_category,sum(duration) as duration\n" +
            "FROM t_log \n" +
            "WHERE user = #{username}\n" +
            "  AND date_time >= #{startDate} and date_time <= #{endDate}\n" +
            "GROUP BY large_category;")
    List<Log> selectByRange(String username , LocalDate startDate, LocalDate endDate);

    @Select("SELECT ${field},sum(duration) as duration FROM `t_log` " +
            "where user=#{username}  " +
            "AND date_time >= #{startDate} " +
            "and date_time <= #{endDate} GROUP BY ${field};")
    List<Log> selectQualityByRange(String field,String username , LocalDate startDate, LocalDate endDate);




    @Select("\t\tSELECT \n" +
            "    large_category,\n" +
            "    YEAR(date_time) AS year,\n" +
            "    WEEK(date_time, 1) AS week_number,\n" +
            "    SUM(duration)/60 AS duration\n" +
            "FROM \n" +
            "    t_log\n" +
            "WHERE \n" +
            "    user = #{username} \n" +
            "    AND date_time >= NOW() - INTERVAL 52 WEEK\n" +
            "GROUP BY \n" +
            "    large_category,\n" +
            "    YEAR(date_time),\n" +
            "    WEEK(date_time, 1)\n" +
            "ORDER BY \n" +
            "    year, \n" +
            "    week_number;")
    List<WeekBean> getWeekList(String username);


    @Select("SELECT IFNULL(SUM(duration), 0)  FROM `t_log` where `user`=#{username} and large_category=#{largeCategory} and date_time BETWEEN #{startDate} and #{endDate}")
    int getSumLargeCategory(String username,String largeCategory,LocalDate startDate,LocalDate endDate);
    @Select("SELECT IFNULL(SUM(duration), 0)  FROM `t_log` where `user`=#{username} and type=#{type} and date_time BETWEEN #{startDate} and #{endDate}")
    int getSumType(String username,String type,LocalDate startDate,LocalDate endDate);
    @Select("SELECT IFNULL(SUM(duration), 0)  FROM `t_log` where `user`=#{username} and project=#{project} and date_time BETWEEN #{startDate} and #{endDate}")
    int getSumProject(String username,String project,LocalDate startDate,LocalDate endDate);
}

