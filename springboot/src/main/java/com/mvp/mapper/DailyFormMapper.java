package com.mvp.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.DailyForm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface DailyFormMapper extends BaseMapper<DailyForm> {

    @Select("select * from daily_form where id=#{prototypeId} and name = #{name} and username=#{username} and type =#{type}")
    public DailyForm selectDailyFormByFourField(int prototypeId,String name,String username,String type);


    @Update("update daily_form set deleted=1 where id = #{prototypeId}")
    public Integer deleteByPrototypeId(int prototypeId);
}