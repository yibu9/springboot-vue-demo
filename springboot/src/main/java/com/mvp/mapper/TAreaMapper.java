package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.TArea;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface TAreaMapper extends BaseMapper<TArea> {

    @Select("SELECT " +
            "t_area.id AS grandson_id, " +
            "t_area.area_name AS grandson_area_name, " +
            "parent_area.area_name AS father_area_name, " +
            "grandparent_area.area_name AS grandfather_area_name " +
            "FROM t_area " +
            "LEFT JOIN t_area AS parent_area ON t_area.parent_id = parent_area.id " +
            "LEFT JOIN t_area AS grandparent_area ON parent_area.parent_id = grandparent_area.id " +
            "WHERE t_area.node_type = 1")
    List<Map<String, Object>> getRelatedAreaNamesBySql();
}