package com.mvp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mvp.entity.Journal;


public interface JournalMapper extends BaseMapper<Journal> {

}
