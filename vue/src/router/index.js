import { createRouter, createWebHistory } from 'vue-router'
import Layout from "@/layout/Layout.vue";

const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: '/log',
    children: [
      {
        path: '/user',
        name: 'User',
        component: ()=>import("@/views/UserView.vue")
      },
      {
        path: '/person',
        name: 'Person',
        component: ()=>import("@/views/PersonView.vue")
      },
      {
        path: '/book',
        name: 'Book',
        component: ()=>import("@/views/BookView.vue")
      },
      {
        path: '/news',
        name: 'News',
        component: ()=>import("@/views/NewsView.vue")
      },
      {
        path: '/log',
        name: 'Log',
        component: ()=>import("@/views/LogView.vue")
      },
      {
        path: '/stat',
        name: 'Stat',
        component: ()=>import("@/views/StatView.vue")
      },
      {
        path: '/plan',
        name: 'Plan',
        component: ()=>import("@/views/PlanView.vue")
      },
      {
        path: '/habit',
        name: 'Habit',
        component: ()=>import("@/views/HabitView.vue")
      },
      {
        path: '/project',
        name: 'Project',
        component: ()=>import("@/views/ProjectView.vue")
      },
      {
        path: '/report',
        name: 'Report',
        component: ()=>import("@/views/ReportView.vue")
      },
      {
        path: '/level',
        name: 'Level',
        component: ()=>import("@/views/LevelView.vue")
      },
      {
        path: '/journal',
        name: 'Journal',
        component: ()=>import("@/views/JournalView.vue")
      },
      {
        path: '/task',
        name: 'Task',
        component: ()=>import("@/views/TaskView.vue")
      },
      {
        path: '/progress',
        name: 'Progress',
        component: ()=>import("@/views/ProgressView.vue")
      },        {
        path: '/progressStat',
        name: 'progressStat',
        component: ()=>import("@/views/ProgressStatView.vue")
      },      {
        path: '/history',
        name: 'History',
        component: ()=>import("@/views/HistoryView.vue")
      },      {
        path: '/statType',
        name: 'StatType',
        component: ()=>import("@/views/StatTypeView.vue")
      },      {
        path: '/statQuality',
        name: 'statQuality',
        component: ()=>import("@/views/StatQualityView.vue")
      },{
        path: '/statWeek',
        name: 'statWeek',
        component: ()=>import("@/views/StatWeekView.vue")
      },{
        path: '/taskManage',
        name: 'taskManage',
        component: ()=>import("@/views/TaskManageView.vue")
      },{
        path: '/buildDailyForm',
        name: 'buildDailyForm',
        component: ()=>import("@/views/dailyform/BuildDailyForm.vue")
      },{
        path: '/dailyForm',
        name: 'dailyForm',
        component: ()=>import("@/views/dailyform/DailyForm.vue")
      },{
        path: '/dailyFormStat',
        name: 'dailyFormStat',
        component: ()=>import("@/views/dailyform/DailyFormStat.vue")
      },{
        path: '/question',
        name: 'question',
        component: ()=>import("@/views/QuestionView.vue")
      },{
        path: '/subscriptionItem',
        name: 'subscriptionItem',
        component: ()=>import("@/views/SubscriptionItemView.vue")
      },{
        path: '/circleTarget',
        name: 'circleTarget',
        component: ()=>import("@/views/CircleTargetView.vue")
      },

    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: ()=>import("@/views/LoginView")
  },
  {
    path: '/register',
    name: 'Register',
    component: ()=>import("@/views/RegisterView")
  },
]
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
export default router
